<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
	protected $table = 'localidades';

	protected $fillable = ['id','nombre','id_provincia','id_partido'];
}
