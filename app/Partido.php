<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
	protected $table = 'partidos';

	protected $fillable = ['id','nombre','id_provincia','orden'];
}
