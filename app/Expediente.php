<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
    protected $table = 'expedientes';

	protected $fillable = ['idProyecto','nroCompleto','numero','ano','sesion_id','created_at','updated_at'];

	protected $primaryKey = 'idProyecto';

	public function sesion()
    {
        return $this->belongsTo('Reintegros\Sesion');
    }

}
