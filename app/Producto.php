<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';

	protected $fillable = ['id','empresa_id','nombre','tipo_registro','nrproducto','nresolucion','marca','certificacion','vigente','created_at','updated_at'];
}
