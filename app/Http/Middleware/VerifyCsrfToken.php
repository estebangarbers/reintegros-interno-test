<?php

namespace Reintegros\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = ['/', 'solicitud', 'solicitud/test', 'solicitud/respuesta', 'solicitud/cronCrearProyectos', 'solicitud/cronConsultarExpediente', 'solicitud/cronCrearGedos', 'solicitud/cronVincularGedosAExpediente', 'solicitud/cronCleanSesiones'];
}
