<?php

namespace Reintegros\Http\Controllers;

use Reintegros\Provincia;
use Illuminate\Http\Request;

class ProvinciaController extends Controller
{
    public static function getAll(){
    	$provincias = Provincia::all();
        $select_provincia = [];
        $select_provincia[''] = "Seleccione una provincia";
        foreach($provincias as $provincia){
            $select_provincia[$provincia->id] = $provincia->nombre;
        }
        return $select_provincia;
    }
}
