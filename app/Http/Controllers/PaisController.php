<?php

namespace Reintegros\Http\Controllers;

use Reintegros\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public static function getAll(){
    	$paises = Pais::all();
        $select_pais = [];
        $select_pais[''] = "Seleccione un país de destino";
        foreach($paises as $pais){
            $select_pais[$pais->alpha_3] = $pais->nombre;
        }
        return $select_pais;
    }
}
