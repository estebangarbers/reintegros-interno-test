<?php

namespace Reintegros\Http\Controllers;

use Illuminate\Http\Request;
use Reintegros\Http\Controllers\RestRequest;

class LinkGDE extends Controller
{
    protected $codigoProyecto;
    protected $urlProyecto;
    protected $urlResponse;
    protected $urlCrearDocumento;
    protected $vincularDocumento;
    protected $consultarExpediente;

    /*
    ws_gde_usuario_existe: 'http://linkgde.magyp.gob.ar/usuarios/getusuario' // pa que lo tengas
    */

    public function __construct() {
        $this->codigoProyecto = 'TESTANSES';  //"TESTANSES" 
        $this->urlProyecto = 'http://test-proyectos.magyp.gob.ar/api/WSProyectos/SolicitudProyecto';
        $this->urlResponse = 'http://test-alimentosargentinos.magyp.gob.ar/reintegros/solicitud/respuesta';
        $this->urlCrearDocumento = 'http://test-linkgde.magyp.gob.ar/Documento/GenerarDocumento'; // 'http://magyp-iis-desa.magyp.ar:9090/Documento/GenerarDocumento'
        $this->urlCrearDocumentoFC = 'http://test-linkgde.magyp.gob.ar/Documento/GenerarDocumentoFC';
        $this->vincularDocumento = 'http://test-linkgde.magyp.gob.ar/Documento/VincularDocumento'; // 'http://magyp-iis-desa.magyp.ar:9090/documento/VincularDocumentoYConfirmar'
        $this->consultarExpediente = 'http://test-proyectos.magyp.gob.ar/api/WSProyectos/GetExpediente';
    }

    // CREA CARATULA - DEVUELVE IDPROYECTO
    public function crearProyecto($sesion, $usuario) {
        $datosGDE = [
            'titulo' => 'Solicitud de reintegro a las exportaciones para Sello AA e IGDO',
            'texto' => 'Solicitud de Reintegro EXP Nro '. $sesion->id,
            'areaDestinataria' => 'DGPA#MPYT', // CORROBORAR
            'codigoTramite' => $this->codigoProyecto,
            'externo' => 'true',
            'cuit' => $sesion->empresa->cuit,
            'usuario' => strtoupper($usuario),
            'sistema' => 'SELLOS', // ??????????
            'token' => 'NO85190UNn1n06VF5C3sd5bt7N98m0Mm9U0iJM8ny78Tb67T6fg5rDe3cS254V65',
            'urlResponse' => $this->urlResponse,
        ];

        $rest = new RestRequest($this->urlProyecto, 'POST', $datosGDE);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            $data = $respuesta['Data'];
            if ($data['idProyecto'] == -1) {
                return 0;
            }

            return $data['idProyecto'];
        }

        return 0;
    }

    // CREA GEDO
    public function crearDocumentoGedo($archivo, $nombre, $usuarioGDE) {
        if (is_null($usuarioGDE)) {
            return 0;
        }
        $archivoEnBase64 = base64_encode($archivo);
        $datosDocumento = [
            'Reparticion' => 'DI#MA',//'DI#MA',
            'Nombre' => $nombre,//'fijate de poner un nombre copado para cada formulario pdf, para poder identifcarlos',
            'Acronimo' => 'IFGRA', //creeria que no hay que cambiarlo
            'Data' => $archivoEnBase64, //Archivo en base64
            /*"MetaDatos":[
                {
                    "Clave": "txt_cuit_imex",
                    "Valor": "este es un valor de prueba"
                }
            ],*/
            'SistemaOrigen' => 'MA_PROYECTOS',
            'Referencia' => $nombre, // nosotros usamos lo mismo que 'Nombre'
            'TipoArchivo' => 'pdf',
            'Usuario' => strtoupper($usuarioGDE),
            'Cargo' => 'TECNICO', //cargo del usuario
            //'Destinatarios':null
        ];

        dd($datosDocumento);

        $rest = new RestRequest($this->urlCrearDocumento, 'POST', $datosDocumento);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            $data = $respuesta['Data'];
            $datos = [
                'licencia' => $data['licenciaField'], // esto nos vino siempre null
                'numero' => $data['numeroField'], // esto es lo importantisimo
                'numeroEspecial' => $data['numeroEspecialField'], // esto nos vino siempre null 
                'urlArchivoGenerado' => $data['urlArchivoGeneradoField'], // esto creo que es info dentro de GDE
            ];

            return $datos;
        }

        return 0;
    }

    // CREA DOCUMENTO FC
    public function crearDocumentoFC($solicitud, $archivo, $nombre, $usuarioGDE) {
        if (is_null($usuarioGDE)) 
        {
            return 0;
        }

        $certificacion = ($solicitud->producto->certificacion == 1 ? "Sello Alimentos Argentinos" : "Denominación de Origen e Indicación Geográfica");
        
        $archivoEnBase64 = base64_encode($archivo);
        $datosComponente = [
			'txt_razon_social' => $solicitud->empresa->razon_social,
            'txt_domicilio' => $solicitud->domicilio,
            'txt_localidad' => ($solicitud->localidad ? $solicitud->localidad->nombre : ''),
            'txt_provincia' => $solicitud->provincia->nombre,
            'txt_producto' => $solicitud->producto->nombre,
            'txt_marca' => $solicitud->producto->marca,
            'txt_tipo_n_registro' => ($solicitud->producto->tipo_registro ? $solicitud->producto->tipo_registro : ''),
            'txt_n_resolucion' => $solicitud->producto->nresolucion,
            'txt_certificacion' => $certificacion,
            'txt_n_registro' => ($solicitud->producto->nrproducto ? $solicitud->producto->nrproducto : ''),
            'txt_otros_registros' => ($solicitud->otros_registros ? $solicitud->otros_registros : ''),
            'txt_destinacion' => $solicitud->destinacion,
            'txt_repita_destinacion' => $solicitud->destinacion,
            'txt_cantidad_declarada' => $solicitud->cdeclarada,
            'txt_pais_destino' => $solicitud->pais_destino->nombre,
            'txt_posicion_arancelaria' => $solicitud->parancelaria,
            'txt_valor_fob' => $solicitud->vfob,
            'txt_medio_transporte' => $solicitud->mtransporte,
            'txt_nombre_transporte' => $solicitud->ntransporte,
            'txt2500_descripcion' => $solicitud->descripcion,
            'txt_correo_electronico' => $solicitud->email,
            'txt_telefono' => $solicitud->telefono
		];

		$datosDocumento = [
            'Acronimo' => "FOSLL",
            'Referencia' => $nombre,
            'SistemaOrigen' => 'MA_PROYECTOS',
            'Usuario' => 'NKUGLIEN', // cambiar MNIMO en prod
            'Data' => $archivoEnBase64,
            'Componentes' => $datosComponente,
        ];

        $respuesta = json_decode($this->curlDocumentoFC($datosDocumento), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) 
        {
            $data = $respuesta['Data']['return'];
            $datos = [
                'licencia' => $data['licenciaField'], // esto nos vino siempre null
                'numero' => $data['numeroField'], // esto es lo importantisimo
                'numeroEspecial' => $data['numeroEspecialField'], // esto nos vino siempre null 
                'urlArchivoGenerado' => $data['urlArchivoGeneradoField'], // esto creo que es info dentro de GDE
            ];

            return $datos;
        }

        return 0;
    }

    public function vincularGedoAExpediente($numeros, $expediente, $usuarioGDE) {
        $datosDocumento = [
            'Usuario' => strtoupper($usuarioGDE),
            'SistemaOrigen' => 'MA_PROYECTOS',
            'Expediente' => $expediente,
            'Documentos' => $numeros,
        ];
        $rest = new RestRequest($this->vincularDocumento, 'POST', $datosDocumento);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            return true;
        }

        //return $respuesta['Mensaje'];
        return false;
    }

    public function consultarExpediente($idProyecto) {
        $ws = $this->consultarExpediente . "?idProyecto=$idProyecto";
        $rest = (new RestRequest($ws, 'GET', null))->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (array_key_exists('nroCompleto', $respuesta) && null != $respuesta['nroCompleto']) {
            $expediente = [
                'nroCompleto' => $respuesta['nroCompleto'],
                'numero'      => $respuesta['numero'],
                'ano'         => $respuesta['ano'],
            ];
            return $expediente;
        }
        return null;
    }

    public function prueba(){
        return "asd";
    }

    private function curlDocumentoFC($datosDocumento)
    {
        $json = json_encode($datosDocumento);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $this->urlCrearDocumentoFC); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS,$json); 
        $data = curl_exec($ch);  
        curl_close($ch); 
        return $data;
    }

}
