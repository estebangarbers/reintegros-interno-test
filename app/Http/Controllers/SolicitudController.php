<?php
namespace Reintegros\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Reintegros\Http\Controllers\RestRequest;
use Reintegros\Http\Controllers\LinkGDE;

use Reintegros\Pais;
use Reintegros\Provincia;
use Reintegros\Localidad;

use Reintegros\Empresa;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;
use Reintegros\Expediente;
use Reintegros\Sesion;
use Reintegros\Gedo;

use Carbon\Carbon;

use Flash;
use Session;
use PDF;

class SolicitudController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Pagina inicial
    |--------------------------------------------------------------------------
    |
    | A esta pagina debe estar dirigido PAEC, o autogestion segun corresponda.
    |
    */    
    public function inicio()
    {
        try 
        {
            // TRAE SESION CREADA EN MIDDLEWARE
            $cempresas = Empresa::all()->count();
            $csolicitudes = Solicitud::all()->count();
            $cexpedientes = Expediente::all()->count();
            $solicitudes = Solicitud::orderBy('id', 'desc')->take(10)->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404');
        }

        return view('inicio', compact('cempresas','csolicitudes','cexpedientes','solicitudes'));
    }

    public function checkUser(Request $request){
        try 
        {
            $user = \DB::connection('mysql2')->table('intranet')->where('password','=',crypt($request->input('password'), "231"))->where('usuario_email','=',$request->input('email'))->first();
        }
        catch (ModelNotFoundException $e)
        {
            $request->session()->flush();
            return abort(404, 'ERROR.');
        }
        
        if ($user) {
            session(['sesion_email' => $user->usuario_email]);
            return redirect()->route('solicitud.inicio');
        }else{
            return redirect('/login')->with('message', 'Email y/o contraseña incorrectos')->with('flash.class', 'warning');
        }

        
    }

    /*
    |--------------------------------------------------------------------------
    | Pagina inicial
    |--------------------------------------------------------------------------
    |
    | Esta página muestra la solicitudes registradas por el CUIT con sesión activa.
    |
    */ 
    public function solicitudes(Request $request)
    {
        try
        {
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();

        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'Inicie sesión a través de TAD para continuar.');
        }

        // seteo empresa
        $empresa = $sesion->empresa;
        $solicitudes = Solicitud::where('empresa_id','=',$sesion->empresa->id)->orderBy('id', 'DESC')->paginate(10);
        return view('solicitud.listado', compact('empresa', 'solicitudes'));
    }
     
    
    public function index(Request $request)
    {
        try 
        {
            // TRAE SESION CREADA EN MIDDLEWARE
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'Inicie sesión a través de TAD para continuar.');
        }

        // seteo empresa
        $empresa = $sesion->empresa;

        // traigo los establecimientos relacionados de *rexp_establecimientos*
        $establecimientos = $sesion->empresa->establecimientos()->pluck('registro','registro');

        // traigo los productos relacionados de *rexp_productos*
        $productos = collect($sesion->empresa->productos)->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nombre'].' 
            | '.($item['tipo_registro'] && $item['nrproducto'] ? $item['tipo_registro'].''.$item['nrproducto'].' 
            | ' : '').$item['nresolucion'].' 
            | '.$item['marca']];
        });
        

        // traigo las posiciones arancelarias de *rexp_parancelarias*
        $parancelarias = collect(Parancelaria::all())->mapWithKeys(function ($item) {
            return [$item['numero'] => $item['numero'].' 
            | '.$item['producto']];
        });

        // traigo las posiciones arancelarias de *rexp_paises*
        $paises = Pais::where('alpha_3','!=','arg')->pluck('nombre','id');
        // traigo las posiciones arancelarias de *rexp_provincias*
        $provincias = Provincia::all()->pluck('nombre','id');

        if ($request->copy=='true' && $request->session()->get('post')) {
            $post = collect($request->session()->get('post'));
        }else{
            $post = null;
        }

        return view('solicitud.index', compact('empresa','establecimientos','productos','parancelarias','paises', 'provincias','post'));
    }

    /*
    |--------------------------------------------------------------------------
    | Carga de solicitud
    |--------------------------------------------------------------------------
    |
    | Se carga la solicitud enviada por el usuario.
    | Se carga el GEDO una vez la solicitud esté en BD mediante Formulario Controlado (FC)
    |
    */
    public function store(Request $request)
    {
        if ($request->input() && $request->session()->get('sesion_id')) 
        {
            try 
            {
                $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();
            }
            catch (ModelNotFoundException $e)
            {
                return abort(404, 'ERROR 404 – Ocurrió un error con la sesión.');
            }

            $solicitud = new Solicitud();
            $solicitud->empresa_id = $sesion->empresa_id;
            $solicitud->domicilio = $request->input('domicilio');
            $solicitud->localidad_id = $request->input('localidad_id');
            $solicitud->provincia_id = $request->input('provincia_id');
            $solicitud->producto_id = $request->input('producto_id');
            $solicitud->nrestablecimiento = $request->input('nrestablecimiento');
            $solicitud->cdeclarada = str_replace(",", ".", 
                                     str_replace(".", "", $request->input('cdeclarada')));
            $solicitud->paisdestino_id = $request->input('paisdestino_id');
            $solicitud->parancelaria = $request->input('parancelaria');
            $solicitud->descripcion = $request->input('descripcion');
            $solicitud->mtransporte = $request->input('mtransporte');
            $solicitud->ntransporte = $request->input('ntransporte');
            $solicitud->vfob = str_replace(",", ".", 
                               str_replace(".", "", $request->input('vfob')));
            $solicitud->otros_registros = $request->input('otros_registros');
            $solicitud->destinacion = $request->input('destinacion');
            $solicitud->email = $request->input('email');
            $solicitud->telefono = str_replace("-", "", $request->input('ctelefono'))."-".str_replace("-", "", $request->input('ntelefono'));
            $solicitud->sesion_id = $request->session()->get('sesion_id');
            $result = $solicitud->save();

            if(!$result)
            {
                return abort(500, 'ERROR 500 – Ocurrió un error en el servidor al intentar cargar la solicitud.');
            }

            // Creo FC de la solicitud cargada
            //$this->crearFC($solicitud);

            // Guardo post en sesion por si quiere copiar la solicitud para generar otra similar sin necesidad de completar todo repetitivamente.
            //session()->push('post',collect($request->input()));
            session(['post' => $request->input()]);

            return redirect('solicitud/envio');
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Mensaje satisfactorio de registro de la solicitud
    |--------------------------------------------------------------------------
    |
    | Lo único que hace es devolver una vista con mensaje satisfactorio.
    | Para evitar mantener el POST mediante la redireccion.
    |
    */
    public function envio(Request $request)
    {
        try
        {
            $sesion = Sesion::find($request->session()->get('sesion_id'))->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un error con la sesión.');
        }

        Flash::success('La solicitud ha sido registrada correctamente.');
        $empresa = $sesion->empresa;
        // ENVIAR MAIL?
        //dd($request->session()->get('post'));

        return view('solicitud.envio', compact('empresa'));
    }

    /*
    |--------------------------------------------------------------------------
    | Respuesta GDE
    |--------------------------------------------------------------------------
    |
    | Se recibe la respuesta del sistema. La funcion busca el idProyecto y le carga los 3 datos 
    | recibidos [nroCompleto, numero, ano].
    | Chequea que todas las solicitudes tengan su GEDO. Si esto es afirmativo, vincula todos los 
    | gedos al expediente recibido.
    |
    */
    public function respuesta(Request $request) 
    {
        $params = [];
        $content = $request->getContent();

        if (!empty($content)) 
        {
            $params = json_decode($content, true);
            
            if(JSON_ERROR_NONE == json_last_error() && is_array($params))
            {
                $idProyecto = $params['idProyecto'];
                try 
                {
                    $expediente = Expediente::where('idProyecto', '=', $idProyecto)->firstOrFail();
                }
                catch (ModelNotFoundException $e)
                {
                    return null;
                }
                                
                // MODIFICO CON LA INFORMACION RECIBIDA DEL SISTEMA
                $expediente->nroCompleto = $params['nroCompleto'];
                $expediente->numero = $params['numero'];
                $expediente->ano = $params['ano'];
                $save = $expediente->save();

                if (!$save) return null;

                // CHEQUEO QUE TODAS LAS SOLICITUDES TENGAN GEDO, SI ES TRUE VINCULO GEDO A EXP
                $solicitudes = $expediente->sesion->solicitudes;
                $verificarGedos = $this->checkSolicitudesConGedo($solicitudes);
                if($verificarGedos)
                {
                    foreach ($solicitudes as $solicitud) 
                    {
                        $this->crearFC($solicitud->gedo, $expediente->nroCompleto);
                    }
                }
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para consultar estado del Expediente
    |--------------------------------------------------------------------------
    |
    | Se consulta manualmente si ya se creo el expediente y nuestro sistema no fue notificado.
    | En caso afirmativo, modifica la fila de nuestra BD con la informacion, chequea que esten todos los gedos y
    | vincula los gedos al expediente
    |
    */
    public function cronConsultarExpediente()
    {
        // Selecciona expedientes que no esten completos, es decir, que no hayan recibido respuesta
        try 
        {
            $expedientes = Expediente::where('nroCompleto', '=', '')
                                ->orWhereNull('nroCompleto')
                                ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if ($expedientes) 
        {
            foreach ($expedientes as $expediente)
            {
                $params = $this->linkgde->consultarExpediente($expediente->idProyecto);
                if (is_array($params))
                {
                    $expediente->nroCompleto = $params['nroCompleto'];
                    $expediente->numero = $params['numero'];
                    $expediente->ano = $params['ano'];
                    $expediente->save();
                }
            }   
        }

        return null;  
    }

    /*
    |------------------------------------------------------------------------------------------
    | CRONJOB para crear Proyecto y eliminar la sesiones finalizadas sin solicitudes vinculadas
    |------------------------------------------------------------------------------------------
    |
    | Selecciona las sesiones que NO tengan expediente en BD pero que SI tengan solicitud/es.
    | Crea los proyectos que falten y los guarda en la base de datos con su idProyecto.
    |
    */
    public function cronCrearProyectos(Request $request)
    {
        // selecciona las sesiones con mas de 30 minutos de inactividad, que NO tengan proyecto iniciado, pero SI tengan solicitudes creadas.
        try
        {
            $sesiones = Sesion::where('last_activity','<',Carbon::now()->subMinutes(31)->timestamp)
            ->whereNotIn('id', function($q)
            {
                $q->select('sesion_id')->from('expedientes');
            })
            ->whereIn('id', function($q)
            {
                $q->select('sesion_id')->from('solicitudes');
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }
        
        if (!$sesiones->isEmpty())
        {
            foreach ($sesiones as $sesion) 
            {
                // CREO PROYECTO
                $this->crearProyecto($sesion);
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para limpiar sesiones almacenadas en base de datos
    |--------------------------------------------------------------------------
    |
    | Selecciona las sesiones que caducaron sin solicitudes vinculadas y las borra.
    |
    */
    public function cronCleanSesiones()
    {
        try
        {
            /* SELECT SESIONES FINALIZADAS SIN SOLICITUDES VINCULADAS */
            $sesiones = Sesion::where('last_activity','<',Carbon::now()->subMinutes(31)->timestamp)
            ->whereNotIn('id', function($q)
            {
                $q->select('sesion_id')->from('expedientes');
            })
            ->whereNotIn('id', function($q)
            {
                $q->select('sesion_id')->from('solicitudes');
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$sesiones->isEmpty())
        {
            foreach ($sesiones as $sesion) 
            {
                $sesion->delete();
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para crear FC (GEDOS)
    |--------------------------------------------------------------------------
    |
    | Selecciona las solicitudes creadas sin GEDO vinculado.
    | Crea los FC correspondientes.
    |
    */
    public function cronCrearGedos(Request $request)
    {
        try
        {
            $solicitudes = Solicitud::whereNotIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos');
            })->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$solicitudes->isEmpty()) 
        {
            foreach ($solicitudes as $solicitud) 
            {
                $this->crearFC($solicitud);
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para vincular GEDOS a Expediente
    |--------------------------------------------------------------------------
    |
    | Selecciona sesiones con más de 30 minutos de inactividad, que tengan creado expediente con su 
    | numero completo, con todas sus solicitudes con su respectivo GEDO (FC) y que no esten vinculados 
    | ("vinculado" = 0).
    | Con las sesiones seleccionadas, recorre todas las solicitudes generadas en cada sesión y las vincula 
    | una por una.
    | Esto se repita hasta que la sesión tenga expediente completo, todas las solicitudes con GEDO 
    | y todo GEDO vinculado.
    |
    */
    public function cronVincularGedosAExpediente()
    {
        try
        {
            $sesiones = Sesion::where('last_activity','<',Carbon::now()->subMinutes(31)->timestamp)
            ->whereIn('id', function($q)
            {
                $q->select('sesion_id')->from('expedientes')
                ->where('nroCompleto','!=',"")
                ->orWhereNotNull('nroCompleto');
            })
            ->whereIn('id', function($q)
            {
                $q->select('sesion_id')->from('solicitudes')
                ->join('gedos', 'solicitudes.id', '=', 'gedos.solicitud_id')
                ->where('gedos.vinculado','=',0);
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$sesiones->isEmpty()) 
        {
            foreach ($sesiones as $sesion) 
            {
                $nroExpediente = $sesion->expediente->nroCompleto;
                $solicitudes = $sesion->solicitudes;
                $checkGedos = $this->checkSolicitudesConGedo($solicitudes);
                if ($checkGedos) 
                {
                    $gedos = array();                    
                    foreach ($solicitudes as $solicitud) 
                    {
                        $gedos[] = $solicitud->gedo->numero;
                    }

                    $gedosAVincular = json_encode($gedos,true);

                    /*                    
                    $result = $linkgde->vincularGedoAExpediente($gedosAVincular, $nroExpediente, $this->usuarioGDE);
                    if ($result) 
                    {
                        foreach ($solicitudes as $solicitud) 
                        {
                            $solicitud->gedo->vinculado = 1;
                            $solicitud->gedo->save();
                        }
                    }
                    */
                    
                    echo $nroExpediente.":<br>";
                    echo $gedosAVincular."<br><br>";
                }
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | Crear Proyecto (Expediente)
    |--------------------------------------------------------------------------
    |
    | Crea el proyecto en la BD, guarda el numero "idProyecto" que servirá de referencia cuando el 
    | sistema reciba la respuesta.
    |
    */
    private function crearProyecto($sesion)
    {
        $idProyecto = $this->linkgde->crearProyecto($sesion, $this->usuarioGDE);
        if ($idProyecto != 0)
        {
            $expediente = new Expediente();
            $expediente->idProyecto = $idProyecto;
            $expediente->sesion_id = $sesion->id;
            $save = $expediente->save();
            if($save) return true;
        }

        return false;
    }
   
    /*
    |--------------------------------------------------------------------------
    | Crear Formularios Controlados
    |--------------------------------------------------------------------------
    |
    | Recibe el objeto solicitud, crea PDF y envia la información a linkGDE para crear el FC.
    | Retorna json con el numero de FC creado, el mismo se inserta en base de datos con vinculacion 
    | a la solicitud en cuestion. 
    |
    */
    private function crearFC($solicitud)
    {
        if (!$solicitud->gedo)
        {
            $nombre = "Solicitud de Certificado de Reintegro Nro ".str_pad($solicitud->id, 8, "0", STR_PAD_LEFT);

            $pdf = PDF::loadView('pdf.solicitudes', compact('solicitud'));
            $archivo = $pdf->output();
            //$archivo = null;
            $data = $this->linkgde->crearDocumentoFC($solicitud, $archivo, $nombre, $this->usuarioGDE);
            
            if($data)
            {
                $gedo = new Gedo();
                $gedo->licencia = $data['licencia'];
                $gedo->numero = $data['numero'];
                $gedo->numeroEspecial = $data['numeroEspecial'];
                $gedo->urlArchivoGenerado = $data['urlArchivoGenerado'];
                $gedo->solicitud_id = $solicitud->id;
                $save = $gedo->save();
                if ($save) return true;
            }
        }

        return false;      
    }

    /*
    |--------------------------------------------------------------------------
    | Vincular FC a Expediente
    |--------------------------------------------------------------------------
    | Recibe el objeto solicitud y el numero de expediente (String)
    | Si es linkGDE retorna true, modifica el estado del GEDO por 1 (Vinculado) en base de datos
    |
    */
    private function vincularGedoExpediente($solicitud, $expediente)
    {
        if ($solicitud->gedo->numero && $expediente) 
        {
            $vinculacion = $linkgde->vincularGedoAExpediente($solicitud->gedo->numero, 
                                                            $expediente, $this->usuarioGDE);
            
            if ($vinculacion) 
            {
                $solicitud->gedo->vinculado == 1;
                $solicitud->gedo->save();
                return true;
            }
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | Chequear solicitudes con GEDO
    |--------------------------------------------------------------------------
    |
    | Recibe array de solicitudes. Devuelve FALSE cuando hay al menos 1 solicitud sin GEDO
    |
    */
    private function checkSolicitudesConGedo($solicitudes)
    {
        foreach ($solicitudes as $solicitud) 
        {
            if (!$solicitud->gedo) 
            {
                return false;
            }
        }
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Buscar localidad
    |--------------------------------------------------------------------------
    |
    | Busca localidades con los parametros recibidos p y q. p => Provincia, q => Query
    | Función que se utiliza para seleccionar localidad en el formulario de la solicitud.
    |
    */
    public function searchLocalidad(Request $request)
    {
        if ($request->p && $request->q) 
        {
            $localidades = \DB::table('localidades')
                ->join('partidos', 'partidos.id', '=', 'localidades.partido_id')
                ->select('localidades.id as localidad_id','localidades.nombre as localidad_nombre','partidos.id as partido_id','partidos.nombre as partido_nombre')
                ->where('partidos.provincia_id','=',$request->p)
                ->where('localidades.nombre','LIKE','%'.$request->q.'%')
                ->get();

            return json_encode($localidades);
        }
        
        return null;
    }

    public function view(Request $request){
        try
        {
            $solicitud = Solicitud::where('id','=',$request->solicitud_id)->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        $pdf = PDF::loadView('pdf.solicitudes', compact('solicitud'));
        return $pdf->download($solicitud->empresa->cuit."-".$solicitud->id.".pdf");
    }

    public function certificado(Request $request){
        try
        {
            $solicitud = Solicitud::where('id','=',$request->solicitud_id)->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        $pdf = PDF::loadView('pdf.certificados', compact('solicitud'));
        return $pdf->download($solicitud->empresa->cuit."-".$solicitud->id."-certificado.pdf");
    }

    public function copy(Request $request)
    {
        try
        {
            $solicitud = Solicitud::where('id','=',$request->solicitud_id)->firstOrFail(); 
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if ($solicitud->localidad) 
        {
           $solicitud->q = $solicitud->localidad->nombre.", ".$solicitud->localidad->partido->nombre;
        }
        else
        {
            $solicitud->q = null;
        }
        
        $solicitud->rdestinacion = $solicitud->destinacion;
        $telefono = explode('-', $solicitud->telefono, 2);
        $solicitud->ctelefono = $telefono[0];
        $solicitud->ntelefono = $telefono[1];
        session(['post' => $solicitud]);
        return \Redirect::route('solicitud.index', ['copy' => 'true']);
    }

    /*
    |--------------------------------------------------------------------------
    | Ruta para testear
    |--------------------------------------------------------------------------
    */
    public function test(Request $request)
    {
        $solicitud = Solicitud::where('id','=',79)->firstOrFail(); 
        $pdf = PDF::loadView('pdf.certificados', compact('solicitud'));
        //return $pdf->stream();
        $archivo = $pdf->output();
        $result = $this->linkgde->crearDocumentoGedo($archivo,"Reintegros Sellos Alimentos Argentinos",$this->usuarioGDE);

        //$file = $pdf->output();
        //return base64_encode($file);
        //exit;

        /*try
        {
            $solicitudes = Solicitud::whereNotIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos');
            })->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        dd($solicitudes);*/

        /*try
        {
            $sesiones = Sesion::where('last_activity','<',Carbon::now()->subMinutes(31)->timestamp)
            ->whereIn('id', function($q)
            {
                $q->select('sesion_id')->from('expedientes')
                ->where('nroCompleto','!=',"")
                ->orWhereNotNull('nroCompleto');
            })
            ->whereIn('id', function($q)
            {
                $q->select('sesion_id')->from('solicitudes')
                ->join('gedos', 'solicitudes.id', '=', 'gedos.solicitud_id')
                ->where('gedos.vinculado','=',0);
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        dd($sesiones);*/
    }
}
