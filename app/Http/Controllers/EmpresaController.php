<?php
namespace Reintegros\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Reintegros\Http\Controllers\RestRequest;
use Reintegros\Http\Controllers\LinkGDE;

use Reintegros\Pais;
use Reintegros\Provincia;
use Reintegros\Localidad;

use Reintegros\Empresa;
use Reintegros\Producto;
use Reintegros\Establecimiento;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;
use Reintegros\Expediente;
use Reintegros\Sesion;
use Reintegros\Gedo;

use Carbon\Carbon;

use Flash;
use Session;
use PDF;

class EmpresaController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Pagina inicial
    |--------------------------------------------------------------------------
    |
    | A esta pagina debe estar dirigido PAEC, o autogestion segun corresponda.
    |
    */    
    public function index()
    {
        try 
        {
            $empresas = Empresa::orderBy('razon_social', 'asc')->paginate(10);
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404');
        }

        return view('empresas.index', compact('empresas'));
    }

    /*
    |--------------------------------------------------------------------------
    | Pagina inicial
    |--------------------------------------------------------------------------
    |
    | Esta página muestra la solicitudes registradas por el CUIT con sesión activa.
    |
    */ 
    public function solicitudes($id)
    {
        try
        {
            $empresa = Empresa::where('id', '=', $id)->firstOrFail();
            $solicitudes = Solicitud::where('empresa_id', '=', $id)->orderBy('id', 'DESC')->paginate(10);
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR.');
        }

        return view('empresas.solicitudes', compact('empresa', 'solicitudes'));
    }

    public function detalles(Request $request)
    {
        try
        {
            $empresa = Empresa::where('id', '=', $request->id)->firstOrFail();
            $productos = Producto::where('empresa_id','=',$empresa->id)->orderBy('id', 'DESC')->limit(5)->get();
            $solicitudes = Solicitud::where('empresa_id','=',$empresa->id)->orderBy('id', 'DESC')->limit(5)->get();
            $establecimientos = Establecimiento::where('empresa_id','=',$empresa->id)->orderBy('id', 'DESC')->limit(5)->get();

        }
        catch (ModelNotFoundException $e)
        {
            return redirect('/empresas');
        }

        return view('empresas.detalles', compact('empresa','productos','solicitudes','establecimientos'));
    }

    public function nueva()
    {
        return view('empresas.nueva');
    }
     
    
    /*public function index(Request $request)
    {
        try 
        {
            // TRAE SESION CREADA EN MIDDLEWARE
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'Inicie sesión a través de TAD para continuar.');
        }

        // seteo empresa
        $empresa = $sesion->empresa;

        // traigo los establecimientos relacionados de *rexp_establecimientos*
        $establecimientos = $sesion->empresa->establecimientos()->pluck('registro','registro');

        // traigo los productos relacionados de *rexp_productos*
        $productos = collect($sesion->empresa->productos)->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nombre'].' 
            | '.($item['tipo_registro'] && $item['nrproducto'] ? $item['tipo_registro'].''.$item['nrproducto'].' 
            | ' : '').$item['nresolucion'].' 
            | '.$item['marca']];
        });
        

        // traigo las posiciones arancelarias de *rexp_parancelarias*
        $parancelarias = collect(Parancelaria::all())->mapWithKeys(function ($item) {
            return [$item['numero'] => $item['numero'].' 
            | '.$item['producto']];
        });

        // traigo las posiciones arancelarias de *rexp_paises*
        $paises = Pais::where('alpha_3','!=','arg')->pluck('nombre','id');
        // traigo las posiciones arancelarias de *rexp_provincias*
        $provincias = Provincia::all()->pluck('nombre','id');

        if ($request->copy=='true' && $request->session()->get('post')) {
            $post = collect($request->session()->get('post'));
        }else{
            $post = null;
        }

        return view('solicitud.index', compact('empresa','establecimientos','productos','parancelarias','paises', 'provincias','post'));
    }*/

    /*
    |--------------------------------------------------------------------------
    | Carga de solicitud
    |--------------------------------------------------------------------------
    |
    | Se carga la solicitud enviada por el usuario.
    | Se carga el GEDO una vez la solicitud esté en BD mediante Formulario Controlado (FC)
    |
    */
    public function store(Request $request)
    {
        if ($request->input()) 
        {
            try 
            {
                $empresa = Empresa::where('cuit', '=', str_replace("-", "", $request->input('cuit')))->first();
            }
            catch (ModelNotFoundException $e)
            {
                return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar la empresa.');
            }

            if (!$empresa) {
                $empresa = new Empresa();
                $empresa->razon_social = $request->input('razon_social');
                $empresa->cuit = str_replace("-", "", $request->input('cuit'));
                $empresa->activo = $request->input('activo');
                $result = $empresa->save();

                if(!$result)
                {
                    return abort(500, 'ERROR 500 – Ocurrió un problema al intentar registrar la empresa.');
                }

                //session()->flash('message', 'Post was created!');
                return redirect()->route('empresas.detalles',['id' => $empresa->id])->with('message', 'La empresa fue registrada correctamente en el sistema.')->with('flash.class', 'success');
            }else{
                return redirect()->route('empresas.nueva')->with('message', 'El CUIT '.$request->input('cuit').' ya existe en la base de datos.')->with('flash.class', 'danger');;
            }
        }
    }

    public function edit($id)
    {
        $empresa = Empresa::where('id','=',$id)->first();
        return view('empresas.edit', compact('empresa'));
    }

    public function update(Request $request)
    {
        try 
        {
            $empresa = Empresa::where('id', '=', $request->input('empresa_id'))->first();
            $empresa_sel = Empresa::where('id', '<>', $request->input('empresa_id'))->where('cuit', '=',  str_replace("-", "", $request->input('cuit')))->first();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar editar a la empresa.');
        }

        if(!$empresa_sel){
            $empresa->razon_social = $request->input('razon_social');
            $empresa->cuit = str_replace("-", "", $request->input('cuit'));
            $empresa->activo = $request->input('activo');
            $result = $empresa->save();

            if(!$result)
            {
                return abort(500, 'ERROR 500 – Ocurrió un problema al intentar editar a la empresa.');
            }

            return redirect()->route('empresas.edit',['empresa_id' => $empresa->id])->with('message', 'La empresa fue modificada correctamente.')->with('flash.class', 'success');
        }else{
            return redirect()->route('empresas.edit',['empresa_id' => $empresa->id])->with('message', 'Ya existe una empresa con el CUIT '.$request->input('cuit').'.')->with('flash.class', 'danger');
        }

        
    }
}
