<?php

namespace Reintegros\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Reintegros\Http\Controllers\LinkGDE;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $linkgde;
    protected $usuarioGDE;

    /*protected $codigoProyecto;
    protected $urlProyecto;
    protected $urlResponse;
    protected $urlCrearDocumento;
    protected $urlCrearDocumentoFC;
    protected $vincularDocumento;
    protected $consultarExpediente;*/

    public function __construct() {
        $this->linkgde = new LinkGDE();
        $this->usuarioGDE = 'NKUGLIEN';
        /*$this->codigoProyecto = 'TESTANSES';
        $this->urlProyecto = 'http://test-proyectos.magyp.gob.ar/api/WSProyectos/SolicitudProyecto';
        $this->urlResponse = 'http://test-alimentosargentinos.magyp.gob.ar/reintegros/solicitud/respuesta';
        $this->urlCrearDocumento = 'http://test-linkgde.magyp.gob.ar/Documento/GenerarDocumento';
        $this->urlCrearDocumentoFC = 'http://test-linkgde.magyp.gob.ar/Documento/GenerarDocumentoFC';
        $this->vincularDocumento = 'http://test-linkgde.magyp.gob.ar/Documento/VincularDocumento';
        $this->consultarExpediente = 'http://test-proyectos.magyp.gob.ar/api/WSProyectos/GetExpediente';*/
    }
}
