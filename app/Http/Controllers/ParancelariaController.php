<?php

namespace Reintegros\Http\Controllers;

use Illuminate\Http\Request;
use Reintegros\Empresa;
use Reintegros\Establecimiento;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;

class ParancelariaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parancelarias = Parancelaria::orderBy('producto', 'asc')->paginate(10);
        return view('parancelarias.index', compact('parancelarias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nueva()
    {
        //$parancelaria = Parancelaria::where('id','=',$id)->first();
        return view('parancelarias.nueva');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input()) 
        {
            try 
            {
                $parancelaria = Parancelaria::where('numero','=',$request->input('numero'))->first();
            }
            catch (ModelNotFoundException $e)
            {
                return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar la posición arancelaria.');
            }

            if (!$parancelaria) {
                $parancelaria = new Parancelaria();
                $parancelaria->numero = $request->input('numero');
                $parancelaria->producto = $request->input('producto');
                $result = $parancelaria->save();

                if(!$result)
                {
                    return abort(500, 'ERROR 500 – Ocurrió un problema al intentar registrar la posición arancelaria.');
                }

                return redirect()->route('parancelarias')->with('message', 'La posición arancelaria fue registrada correctamente en el sistema.')->with('flash.class', 'success');
            }else{
                return redirect()->route('parancelarias.nueva')->with('message', 'La posición arancelaria con número '.$request->input('numero').' ya existe en la base de datos.')->with('flash.class', 'danger');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parancelaria = Parancelaria::where('id','=',$id)->first();
        return view('parancelarias.edit', compact('parancelaria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try 
        {
            $parancelaria = Parancelaria::where('id','=',$request->input('parancelaria_id'))->first();
            $parancelaria_r = Parancelaria::where('numero','=',$request->input('numero'))->where('id','<>',$request->input('parancelaria_id'))->first();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar la posición arancelaria.');
        }

        if (!$parancelaria_r) {
            $parancelaria->numero = $request->input('numero');
            $parancelaria->producto = $request->input('producto');
            $result = $parancelaria->save();

            if(!$result)
            {
                return abort(500, 'ERROR 500 – Ocurrió un problema al intentar registrar la posición arancelaria.');
            }

            return redirect()->route('parancelarias.edit',['id' => $parancelaria->id])->with('message', 'La posición arancelaria fue modificada correctamente.')->with('flash.class', 'success');
        }else{
            return redirect()->route('parancelarias.edit',['id' => $parancelaria->id])->with('message', 'La posición arancelaria con número '.$request->input('numero').' ya existe en la base de datos.')->with('flash.class', 'danger');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try 
        {
            $parancelaria = Parancelaria::where('id','=',$id)->first();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar eliminar la posición arancelaria.');
        }

        $result = $parancelaria->delete();

        if ($result) {
            return redirect()->route('parancelarias')->with('message', 'La posición arancelaria fue eliminada correctamente del sistema.')->with('flash.class', 'success');
        }else{
           return redirect()->route('parancelarias')->with('message', 'La posición arancelaria no ha podido ser eliminada, inténtelo nuevamente.')->with('flash.class', 'danger'); 
        }        
    }
}
