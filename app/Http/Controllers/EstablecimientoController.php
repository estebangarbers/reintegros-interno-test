<?php

namespace Reintegros\Http\Controllers;

use Illuminate\Http\Request;
use Reintegros\Empresa;
use Reintegros\Establecimiento;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;

class EstablecimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($empresa_id)
    {
        $establecimientos = Establecimiento::where('empresa_id','=',$empresa_id)->orderBy('id', 'asc')->paginate(10);
        $empresa = Empresa::where('id','=',$empresa_id)->first();
        return view('empresas.establecimientos', compact('establecimientos','empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($empresa_id)
    {
        $empresa = Empresa::where('id','=',$empresa_id)->first();
        return view('empresas.establecimientos.nuevo', compact('empresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input()) 
        {
            try 
            {
                $empresa = Empresa::where('id', '=', $request->input('empresa_id'))->first();
                $establecimiento = Establecimiento::where('registro','=',$request->input('nrestablecimiento'))->first();
            }
            catch (ModelNotFoundException $e)
            {
                return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar el establecimiento.');
            }

            if (!$establecimiento) {
                $establecimiento = new Establecimiento();
                $establecimiento->registro = $request->input('nrestablecimiento');
                $establecimiento->empresa_id = $request->input('empresa_id');
                $result = $establecimiento->save();

                if(!$result)
                {
                    return abort(500, 'ERROR 500 – Ocurrió un problema al intentar registrar el establecimiento.');
                }

                //session()->flash('message', 'Post was created!');
                return redirect()->route('empresas.establecimientos',['empresa_id' => $empresa->id])->with('message', 'El establecimiento fue registrado correctamente en el sistema.')->with('flash.class', 'success');
            }else{
                return redirect()->route('empresas.establecimientos.nuevo')->with('message', 'El Establecimiento con número '.$request->input('nrestablecimiento').' ya existe en la base de datos.')->with('flash.class', 'danger');;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($empresa_id, $id)
    {
        $empresa = Empresa::where('id','=',$empresa_id)->first();
        $establecimiento = Establecimiento::where('id','=',$id)->first();
        return view('empresas.establecimientos.edit', compact('empresa','establecimiento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try 
        {
            $empresa = Empresa::where('id', '=', $request->input('empresa_id'))->first();
            $establecimiento = Establecimiento::where('id','=',$request->input('establecimiento_id'))->first();
            $establecimiento_r = Establecimiento::where('registro','=',$request->input('nrestablecimiento'))->where('id','<>',$request->input('establecimiento_id'))->first();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar el establecimiento.');
        }

        if (!$establecimiento_r) {
            $establecimiento->registro = $request->input('nrestablecimiento');
            $result = $establecimiento->save();

            if(!$result)
            {
                return abort(500, 'ERROR 500 – Ocurrió un problema al intentar registrar el establecimiento.');
            }

            //session()->flash('message', 'Post was created!');
            return redirect()->route('empresas.establecimientos.edit',['empresa_id' => $empresa->id, 'establecimiento_id' => $establecimiento->id])->with('message', 'El establecimiento fue modificado correctamente.')->with('flash.class', 'success');
        }else{
            return redirect()->route('empresas.establecimientos.edit',['empresa_id' => $empresa->id, 'establecimiento_id' => $establecimiento->id])->with('message', 'El Establecimiento con número '.$request->input('nrestablecimiento').' ya existe en la base de datos.')->with('flash.class', 'danger');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try 
        {
            $establecimiento = Establecimiento::where('id','=',$id)->first();
            $empresa_id = $establecimiento->empresa_id;
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar el establecimiento.');
        }

        $result = $establecimiento->delete();

        if ($result) {
            return redirect()->route('empresas.establecimientos',['empresa_id' => $empresa_id])->with('message', 'El establecimiento fue eliminado correctamente en el sistema.')->with('flash.class', 'success');
        }else{
           return redirect()->route('empresas.establecimientos',['empresa_id' => $empresa_id])->with('message', 'El establecimiento no ha podido ser eliminado, inténtelo nuevamente.')->with('flash.class', 'danger'); 
        }
        
    }
}
