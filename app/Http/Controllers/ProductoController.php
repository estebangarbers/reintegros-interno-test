<?php

namespace Reintegros\Http\Controllers;

use Illuminate\Http\Request;
use Reintegros\Empresa;
use Reintegros\Producto;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;

use Carbon\Carbon;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($empresa_id)
    {
        $productos = Producto::where('empresa_id','=',$empresa_id)->orderBy('id', 'asc')->paginate(10);
        $empresa = Empresa::where('id','=',$empresa_id)->first();
        return view('empresas.productos', compact('productos','empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($empresa_id)
    {
        $empresa = Empresa::where('id','=',$empresa_id)->first();
        return view('empresas.productos.nuevo', compact('empresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input()) 
        {
            try 
            {
                $empresa = Empresa::where('id', '=', $request->input('empresa_id'))->first();
            }
            catch (ModelNotFoundException $e)
            {
                return abort(404, 'ERROR 404 – Ocurrió un problema al intentar registrar el establecimiento.');
            }

                $producto = new Producto();
                $producto->nombre = $request->input('nombre');
                $producto->marca = $request->input('marca');
                $producto->tipo_registro = ($request->input('tipo_registro') ? $request->input('tipo_registro') : null);
                $producto->nrproducto = ($request->input('nrproducto') ? $request->input('nrproducto') : null);
                $producto->certificacion = $request->input('certificacion');
                $producto->nresolucion = $request->input('nresolucion');
                $producto->empresa_id = $request->input('empresa_id');
                $producto->vigente = Carbon::parse(str_replace('/','-',$request->input('fecha')))->addYears(2)->format('Y-m-d');
                $result = $producto->save();

                if(!$result)
                {
                    return abort(500, 'ERROR 500 – Ocurrió un problema al intentar registrar el producto.');
                }

                //session()->flash('message', 'Post was created!');
                return redirect()->route('empresas.productos',['empresa_id' => $empresa->id])->with('message', 'El producto fue registrado correctamente en el sistema.')->with('flash.class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($empresa_id, $id)
    {
        $empresa = Empresa::where('id','=',$empresa_id)->first();
        $producto = Producto::where('id','=',$id)->first();
        $producto->vigente = Carbon::parse($producto->vigente)->subYears(2);
        return view('empresas.productos.edit', compact('empresa','producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try 
        {
            $empresa = Empresa::where('id', '=', $request->input('empresa_id'))->first();
            $producto = Producto::where('id','=',$request->input('producto_id'))->first();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar editar el producto.');
        }

        $producto->nombre = $request->input('nombre');
        $producto->marca = $request->input('marca');
        $producto->tipo_registro = ($request->input('tipo_registro') ? $request->input('tipo_registro') : null);
        $producto->nrproducto = ($request->input('nrproducto') ? $request->input('nrproducto') : null);
        $producto->certificacion = $request->input('certificacion');
        $producto->nresolucion = $request->input('nresolucion');
        $producto->empresa_id = $request->input('empresa_id');
        $producto->vigente = Carbon::parse(str_replace('/','-',$request->input('fecha')))->addYears(2)->format('Y-m-d');
        $result = $producto->save();

        if(!$result)
        {
            return abort(500, 'ERROR 500 – Ocurrió un problema al intentar editar el producto.');
        }

        //session()->flash('message', 'Post was created!');
        return redirect()->route('empresas.productos.edit',['empresa_id' => $empresa->id, 'producto_id' => $producto->id])->with('message', 'El producto fue modificado correctamente.')->with('flash.class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try 
        {
            $producto = Producto::where('id','=',$id)->first();
            $empresa_id = $producto->empresa_id;
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un problema al intentar eliminar el producto.');
        }

        $result = $producto->delete();

        if ($result) {
            return redirect()->route('empresas.productos',['empresa_id' => $empresa_id])->with('message', 'El producto fue eliminado correctamente en el sistema.')->with('flash.class', 'success');
        }else{
           return redirect()->route('empresas.productos',['empresa_id' => $empresa_id])->with('message', 'El producto no ha podido ser eliminado, inténtelo nuevamente.')->with('flash.class', 'danger'); 
        }
        
    }
}
