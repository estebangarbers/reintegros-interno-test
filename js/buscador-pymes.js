var res;
	
    // GRAFICO
    $("#pyme_seleccionar").change(function () {
        elegido=$(this).val();
        $.ajax({
            type: "POST",
            url: "/HomeAlimentos/Publicaciones/intranet/integral/searchPymes.php",
            data: 'pyme=' + elegido,
            dataType: "json", // Set the data type so jQuery can parse it for you
            success: function (data) {
                res = data;
            }
        });
     });

    $(document).ready(function($) {
        // Workaround for bug in mouse item selection
        $.fn.typeahead.Constructor.prototype.blur = function() {
            var that = this;
            setTimeout(function () { that.hide() }, 250);
        };

        var that = this;

        $('#pyme_seleccionar').typeahead({
            source: function(query, process) {
                var results = _.map(res, function(product) {
                   return product.id_pyme;
                });
                process(results);
            },

            highlighter: function(product) {
                var product = _.find(res, function(p) {
                   return p.id_pyme == product;
                   

                });
                return product.id_pyme + " (" + product.id_pyme + " - " + product.id_pyme + ")";
            },

            updater: function(id_pyme) {
                var product = _.find(res, function(p) {
                    return p.id_pyme == id_pyme;
                });
                that.setSelectedProduct(product);
                return product.id_pyme + " (" + product.id_pyme + " - " + product.id_pyme + ")";
            }

        });

        this.setSelectedProduct = function(product) {
            $('#id_pyme_seleccionada').val(product.id_pyme);            
        }
    })