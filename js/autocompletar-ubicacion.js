var res;
	$(document).ready(function(){
    elegido=$('#provincia_id').val();
    $.ajax({
            type: "POST",
            url: "/HomeAlimentos/Publicaciones/intranet/integral/search.php",
            data: 'provincia=' + elegido,
            dataType: "json", // Set the data type so jQuery can parse it for you
            success: function (data) {
                res = data;
            }
    });
	$("#provincia-digital").change(function () {
        $('#localidad-digital').val("");
		valor=$(this).val();
		if (valor != "") {
			document.getElementById("localidad-digital").disabled = false;
		}else{
			document.getElementById("localidad-digital").disabled = true;
			 $('#localidad-digital').val("");
		}
        $("#provincia-digital option:selected").each(function () {
        elegido=$(this).val();
		$.ajax({
		    type: "POST",
		    url: "/HomeAlimentos/Publicaciones/intranet/integral/search.php",
		    data: 'provincia=' + elegido,
		    dataType: "json", // Set the data type so jQuery can parse it for you
		    success: function (data) {
		        res = data;
		    }
		});
        });
     })	
	});

    $(document).ready(function(){
    $("#pais-digital").change(function () {
        if ($(this).val() == 9) {
            $('#provincia-digital').prop('disabled', false);
            document.getElementById("interior-digital").style.display = "block";
            document.getElementById("exterior-digital").style.display = "none";
            $("#exterior-digital :input").attr("disabled", true);
            $("#provincia-digital").attr("disabled", false);
            $("#cpostal").attr("disabled", false);
        }else if($(this).val() == 0){
            $('#provincia-digital').prop('disabled', true);
            document.getElementById("interior-digital").style.display = "none";
            document.getElementById("exterior-digital").style.display = "none";
            $("#exterior-digital :input").attr("disabled", true);
            $("#provincia-digital").attr("disabled", true);
            $("#cpostal").attr("disabled", true);
            $('#localidad-digital').val('');
        }else{
            $('#provincia-digital').prop('disabled', true);
            $('#localidad-digital').prop('disabled', true);
            $('#localidad-digital').val('');
            document.getElementById("interior-digital").style.display = "none";
            document.getElementById("exterior-digital").style.display = "block";
            $("#exterior-digital :input").attr("disabled", false);
            $("#provincia-digital").attr("disabled", true);
            $("#cpostal").attr("disabled", false);
        }
     }) 

    $("#provincia-digital").change(function () {
        if ($(this).val() == 0) {
            $('#localidad-digital').prop('disabled', true);
        }else{
            $('#localidad-digital').prop('disabled', false);
        }
    })
    });


    // GRAFICO
    $(document).ready(function(){
    elegido=$('#provincia_id').val();
    $.ajax({
            type: "POST",
            url: "/HomeAlimentos/Publicaciones/intranet/integral/search.php",
            data: 'provincia=' + elegido,
            dataType: "json", // Set the data type so jQuery can parse it for you
            success: function (data) {
                res = data;
            }
    });
    $("#provincia-grafico").change(function () {
        $('#localidad-grafico').val("");
        valor=$(this).val();
        if (valor != "") {
            document.getElementById("localidad-grafico").disabled = false;
        }else{
            document.getElementById("localidad-grafico").disabled = true;
             $('#localidad-grafico').val("");
        }
        $("#provincia-grafico option:selected").each(function () {
        elegido=$(this).val();
        $.ajax({
            type: "POST",
            url: "/HomeAlimentos/Publicaciones/intranet/integral/search.php",
            data: 'provincia=' + elegido,
            dataType: "json", // Set the data type so jQuery can parse it for you
            success: function (data) {
                res = data;
            }
        });
        });
     }) 
    });

    $(document).ready(function($) {
        // Workaround for bug in mouse item selection
        $.fn.typeahead.Constructor.prototype.blur = function() {
            var that = this;
            setTimeout(function () { that.hide() }, 250);
        };

        var that = this;

        $('#localidad-digital').typeahead({
            source: function(query, process) {
                var results = _.map(res, function(product) {
                   return product.localidad;
                });
                process(results);
            },

            highlighter: function(product) {
                var product = _.find(res, function(p) {
                   return p.localidad == product;
                   alert(product);
                });
                return product.localidad + " (" + product.nombre_partido + " - " + product.nombre_provincia + ")";
            },

            updater: function(localidad) {
                var product = _.find(res, function(p) {
                    return p.localidad == localidad;
                });
                that.setSelectedProduct(product);
                return product.localidad + " (" + product.nombre_partido + " - " + product.nombre_provincia + ")";
            }

        });

        this.setSelectedProduct = function(product) {
            $('#localidad_id').val(product.id);
            $('#partido_id').val(product.id_partido);
            $('#provincia_id').val(product.provincia_id);
            
        }
    })

    $(document).ready(function(){
    $("#pais-grafico").change(function () {
        if ($(this).val() == 9) {
            $('#provincia-grafico').prop('disabled', false);
            document.getElementById("interior-grafico").style.display = "block";
            document.getElementById("exterior-grafico").style.display = "none";
            $("#exterior-grafico :input").attr("disabled", true);
            $("#provincia-grafico").attr("disabled", false);
        }else{
            $('#provincia-grafico').prop('disabled', true);
            $('#localidad-grafico').prop('disabled', true);
            $('#localidad-grafico').val('');
            document.getElementById("interior-grafico").style.display = "none";
            document.getElementById("exterior-grafico").style.display = "block";
            $("#exterior-grafico :input").attr("disabled", false);
            $("#provincia-grafico").attr("disabled", true);
        }
     }) 
    });