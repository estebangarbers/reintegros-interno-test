// REMITO ANOTACIONES
$(document).ready(function(){
  
  $(document).on('click', '#getFile', function(e){
    
    e.preventDefault();
    
    var uid = $(this).data('id');   // it will get id of clicked row
    $("#modal-class").attr('class', 'modal-dialog');
    $('.modal-title').html('Archivo');
    
    $('#dynamic-content').html(''); // leave it blank before ajax call
    $('#modal-loader').show();      // load ajax loader
    
    $.ajax({
      url: 'getPymeRegistroFile.php',
      type: 'POST',
      data: 'id='+uid,
      dataType: 'html'
    })
    .done(function(data){
      console.log(data);  
      $('#dynamic-content').html('');    
      $('#dynamic-content').html(data); // load response 
      $('#modal-loader').hide();      // hide ajax loader 
    })
    .fail(function(){
      $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Ocurrió un problema, por favor inténtelo nuevamente');
      $('#modal-loader').hide();
    });
    
  });
  
});