// REMITO ANOTACIONES
$(document).ready(function(){
  
  $(document).on('click', '#getVinculo', function(e){
    
    e.preventDefault();
    
    var uid = $(this).data('id');   // it will get id of clicked row
    $("#modal-class").attr('class', 'modal-dialog');
    $('.modal-title').html('Vinculo registrado');
    
    $('#dynamic-content').html(''); // leave it blank before ajax call
    $('#modal-loader').show();      // load ajax loader
    
    $.ajax({
      url: 'getVinculoInstitucion.php',
      type: 'POST',
      data: 'id_vinculo='+uid,
      dataType: 'html'
    })
    .done(function(data){
      console.log(data);  
      $('#dynamic-content').html('');    
      $('#dynamic-content').html(data); // load response 
      $('#modal-loader').hide();      // hide ajax loader 
    })
    .fail(function(){
      $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Ocurrió un problema, por favor inténtelo nuevamente');
      $('#modal-loader').hide();
    });
    
  });
  
});

// REMITO ANOTACIONES
$(document).ready(function(){
  
  $(document).on('click', '#getVinculoInstitucionMiembro', function(e){
    
    e.preventDefault();
    
    var uid = $(this).data('id');   // it will get id of clicked row
    $("#modal-class").attr('class', 'modal-dialog');
    $('.modal-title').html('Vinculo registrado');
    
    $('#dynamic-content').html(''); // leave it blank before ajax call
    $('#modal-loader').show();      // load ajax loader
    
    $.ajax({
      url: 'getVinculoInstitucionMiembro.php',
      type: 'POST',
      data: 'id_vinculo='+uid,
      dataType: 'html'
    })
    .done(function(data){
      console.log(data);  
      $('#dynamic-content').html('');    
      $('#dynamic-content').html(data); // load response 
      $('#modal-loader').hide();      // hide ajax loader 
    })
    .fail(function(){
      $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Ocurrió un problema, por favor inténtelo nuevamente');
      $('#modal-loader').hide();
    });
    
  });
  
});