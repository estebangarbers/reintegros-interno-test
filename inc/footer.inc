<footer class="footer" style="font-size:0.9em;">
    <div class="footer-block buttons">
        <i class="fa fa-info-circle"></i> Consultas al <b>42357</b>
    </div>
    <div class="footer-block author">
        <ul>
            <li>
                <a href="http://www.alimentosargentinos.gob.ar/" target="_blank">Alimentos Argentinos</a>
            </li>
        </ul>
    </div>
</footer>
<script src="js/app.js"></script>