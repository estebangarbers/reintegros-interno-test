<header class="header">
    <div class="header-block header-block-collapse hidden-lg-up">
        <button class="collapse-btn" id="sidebar-collapse-btn">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <div class="header-block header-block-search hidden-sm-down">
        <h3 style="margin:0;">Sistema integral</h3>
    </div>
    <div class="header-block header-block-nav">
        <ul class="nav-profile">
            <li class="profile dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <!--<div class="img" style="background-image: url('assets/genericuser.png')"></div>-->
                    <span class="name"> <?php echo $_SESSION['email_sesion']; ?> </span>
                </a>
                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item" href="/HomeAlimentos/Publicaciones/intranet/">
                    <i class="fa fa-arrow-left icon"></i> Volver a intranet </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/HomeAlimentos/Publicaciones/intranet/logout.php">
                    <i class="fa fa-power-off icon"></i> Cerrar sesión </a>
                </div>
            </li>
        </ul>
    </div>
</header>

<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <img src="http://www.alimentosargentinos.gob.ar/HomeAlimentos/Capacitacion/cursos-virtuales/curso-igdo/dist/img/logo_ministerio.png" style="max-width:87%;">
            </div>
        </div>
        <nav class="menu">
            <ul class="nav metismenu" id="sidebar-menu">
                <li <?php echo ($seccion == 1 ? 'class="active"' : ''); ?>>
                    <a href="/HomeAlimentos/Publicaciones/intranet/integral/"><i class="fa fa-home"></i> Inicio </a>
                </li>
                <li <?php echo ($seccion == 2 ? 'class="active open"' : ''); ?>>
                <a style="cursor:pointer;"><i class="fa fa-group" aria-hidden="true"></i> Usuarios <i class="fa arrow"></i> <span class="badge" style="margin-right:8px;"><?php echo $suscriptos->countUsuarios(); ?></span></a>
                    <ul>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/usuarios.php"><i class="fa fa-filter"></i> Filtrar </a>
                        </li>
                        <?php if ($roles->tieneRol($idUsuario, ROL_INTEGRAL_ADMIN)) { ?>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/usuarioNuevo.php"><i class="fa fa-user-plus"></i> Nuevo usuario </a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>

                <li <?php echo ($seccion == 8 ? 'class="active open"' : ''); ?>>
                <a style="cursor:pointer;"><i class="fa fa-university" aria-hidden="true"></i> Instituciones <i class="fa arrow"></i> <span class="badge" style="margin-right:8px;"><?php echo $suscriptos->countInstituciones(); ?></span></a>
                    <ul>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/instituciones.php"><i class="fa fa-filter"></i> Filtrar </a>
                        </li>
                        <?php if ($roles->tieneRol($idUsuario, ROL_INTEGRAL_ADMIN)) { ?>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/institucionNueva.php"><i class="fa fa-plus"></i> Nueva institución </a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>

                <li <?php echo ($seccion == 7 ? 'class="active open"' : ''); ?>>
                <a style="cursor:pointer;"><i class="fa fa-industry" aria-hidden="true"></i> PyMES <i class="fa arrow"></i> <span class="badge" style="margin-right:8px;"><?php echo $suscriptos->countPymes(); ?></span></a>
                    <ul>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/pymes.php"><i class="fa fa-filter"></i> Filtrar </a>
                        </li>
                        <?php if ($roles->tieneRol($idUsuario, ROL_INTEGRAL_ADMIN)) { ?>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/pymeNueva.php"><i class="fa fa-plus"></i> Nueva PyME </a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>

                <li <?php echo ($seccion == 10 ? 'class="active open"' : ''); ?>>
                <a style="cursor:pointer;"><i class="fa fa-keyboard" aria-hidden="true"></i> Formularios <i class="fa arrow"></i></a>
                    <ul>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/pymes.php"><i class="fa fa-user-tag"></i> Suscripciones </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/pymes.php"><i class="fa fa-chalkboard-teacher"></i> Cursos virtuales </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/pymes.php"><i class="fa fa-calendar-alt"></i> Eventos </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/pymes.php"><i class="fa fa-file-alt"></i> Otros formularios </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/formulario.php"><i class="fa fa-cogs" aria-hidden="true"></i> Configuración </a>
                        </li>
                    </ul>
                </li>

                <li <?php echo ($seccion == 3 ? 'class="active"' : ''); ?>>
                    <a href="/HomeAlimentos/Publicaciones/intranet/integral/publicaciones.php"><i class="fa fa-copy" aria-hidden="true"></i> Publicaciones <span class="badge"><?php echo $suscriptos->countPublicaciones(); ?></span></a>
                </li>
                <?php if ($roles->tieneRol($idUsuario, ROL_INTEGRAL_ADMIN)) { ?>
                <li <?php echo ($seccion == 6 ? 'class="active open"' : ''); ?>>
                <a style="cursor:pointer;"><i class="far fa-envelope"></i> Mailing <i class="fa arrow"></i></a>
                    <ul>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/campaigns.php"><i class="fa fa-envelope"></i> Campañas </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/grupos.php"><i class="fa fa-sitemap"></i> Grupos </a>
                        </li>
                    </ul>
                </li>
                
                <li <?php echo ($seccion == 4 ? 'class="active"' : ''); ?>>
                    <a href="/HomeAlimentos/Publicaciones/intranet/integral/origenes.php">
                        <i class="fa fa-cubes"></i> Orígenes </a>
                </li>
                <?php } ?>
                <li <?php echo ($seccion == 9 ? 'class="active open"' : ''); ?>>
                <a style="cursor:pointer;"><i class="fa fa-wrench" aria-hidden="true"></i> Herramientas <i class="fa arrow"></i></a>
                    <ul>
                        <?php if ($roles->tieneRol($idUsuario, ROL_INTEGRAL_ADMIN)) { ?>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/carga_masiva.php"><i class="fa fa-upload"></i> Carga masiva </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/etiquetas.php"><i class="fa fa-print"></i> Generar etiquetas </a>
                        </li>
                        <li>
                            <a href="/HomeAlimentos/Publicaciones/intranet/integral/revision.php"><i class="fa fa-exclamation-triangle"></i> Revisión de registros </a>
                        </li>                         
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <div style="position:absolute;bottom:0;left:0;width: 230px;font-size:0.7em;background:#0595d5;padding-bottom:10px;color:#FFFFFF;border-top:1px solid #0f729e;">
        <div class="col-md-12" style="border-top: 1px solid #23afed;padding-top:10px;">
            <b>Ministerio de Agroindustria<br>Secretaría de Alimentos y Bioeconomía</b><br>D.N. de Alimentos y Bebidas
        </div> 
    </div> 
</aside>