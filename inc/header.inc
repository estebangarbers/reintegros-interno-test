<!--<img src="http://www.alimentosargentinos.gob.ar/aus/imagenes/mantenimiento.jpg" style="max-width:100%;" alt="">-->
<?php 
session_start();
if(!isset($_SESSION['login'])){
    header('Location: ../login.php');
    session_destroy();
    exit;
}
$idUsuario = $_SESSION['idUsuario'];
require_once "../modelo/roles.inc";
require_once "../modelo/connLogin.inc";
require_once "../modelo/roles2_.inc";
$roles = new Roles($mysqlLogin);
?>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Sistema integral | Dirección Nacional de Alimentos y Bebidas</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->
<link rel="stylesheet" href="/HomeAlimentos/Publicaciones/intranet/integral/css/vendor.css">
<link rel="stylesheet" href="/HomeAlimentos/Publicaciones/intranet/integral/css/app.css">
<link rel="stylesheet" href="/HomeAlimentos/Publicaciones/intranet/integral/css/spinner.css">
<link href="/HomeAlimentos/Publicaciones/intranet/integral/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link rel='stylesheet' id='cssmap-argentina-css'  href='assets/mapa-argentina/cssmap-argentina.css?ver=5.5.2' type='text/css' media='screen' />

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>  
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>-->
<link rel="stylesheet" type="text/css" href="/HomeAlimentos/Publicaciones/intranet/integral/css/multi-select.css">
<link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
<style type="text/css" media="screen">
    body{
        font-family: 'Roboto';
    }
    .pagination>li {
        display: inline;
    }
    .pagination>li.active>a, .pagination>li.active>a:focus, .pagination>li.active>a:hover {
        z-index: 2;
        color: #fff;
        cursor: default;
        background-color: #0275d8;
        border-color: #0275d8;
    }
    .pagination>li>a {
        position: relative;
        float: left;
        padding: 0.5rem 0.75rem;
        margin-left: -1px;
        color: #0275d8;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    .pagination>li:first-child>a {
        margin-left: 0;
        border-bottom-left-radius: 0.25rem;
        border-top-left-radius: 0.25rem;
    }
    .pagination>li:last-child>a {
        border-bottom-right-radius: 0.25rem;
        border-top-right-radius: 0.25rem;
    }
    .badge {
        display: inline-block;
        float:right;
        min-width: 10px;
        padding: 3px 7px;
        font-size: 12px;
        font-weight: 700;
        line-height: 1.4;
        color: #777;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        background-color: #d7dde4;
        border-radius: 10px;
    }
    .badge-h4 {
        display: inline-block;
        float:left;
        min-width: 10px;
        padding: 3px 7px;
        font-size: 12px;
        font-weight: 700;
        line-height: 1.4;
        color: #777;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        background-color: #d7dde4;
        border-radius: 10px;
    }
    .table>tbody>tr>td{
        vertical-align: middle;
    }
    .badge-primary{
        background:#23afed;
        color:#FFFFFF;
    }
    .badge-warning{
        background:#fe974b;
        color:#FFFFFF;
    }
    .badge-default{
        background:#ccc;
        color:#FFFFFF;
    }
    .alert.alert-gray {
        background-color: #ECF0F1;
        border-color: #757D75;
        color: #757D75;
    }
    .label-success {
        background-color: #5cb85c;
    }
    .label-warning {
        background-color: #fe974b;
    }
    .label-danger {
        background-color: #d9534f;
    }
    .label {
        display: inline;
        padding: 5px 10px;
        font-size: 1em;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
    }
    footer{
        margin-top:20px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>