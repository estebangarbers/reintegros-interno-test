<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpedientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expedientes', function (Blueprint $table) {
            $table->integer('idProyecto')->unique();
            $table->string('nroCompleto')->nullable();
            $table->string('numero')->unique()->nullable();
            $table->string('ano')->nullable();
            $table->integer('sesion_id')->index()->unsigned();
            $table->foreign('sesion_id')
            ->references('id')->on('sesions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expedientes');
    }
}
