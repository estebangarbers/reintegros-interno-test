<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGedosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gedos', function (Blueprint $table) {
            $table->string('licencia')->nullable();
            $table->string('numero',30)->index()->unique();
            $table->string('numeroEspecial')->nullable();
            $table->string('urlArchivoGenerado')->nullable();
            $table->integer('solicitud_id')->index()->unique()->unsigned();
            $table->foreign('solicitud_id')
            ->references('id')->on('solicitudes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gedos');
    }
}
