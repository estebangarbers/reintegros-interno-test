<?php
use Illuminate\Database\Seeder;
use Reintegros\Parancelaria;

class ParancelariaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('parancelarias')->delete();
        $parancelarias = array(
            array('numero' => '0409.00.00','producto' => 'Miel natural'),
            array('numero' => '0705.21.00','producto' => 'Endivias'),
            array('numero' => '0802.51.00','producto' => 'Pistacho con cascara'),
            array('numero' => '0802.52.00','producto' => 'Pistacho sin cascara'),
            array('numero' => '0805.10.00','producto' => 'Naranjas'),
            array('numero' => '0805.21.00','producto' => 'Mandarinas - Satsuma'),
            array('numero' => '0805.22.00','producto' => 'Mandarinas - Clementina'),
            array('numero' => '0805.29.00','producto' => 'Mandarinas - Demás variedades'),
            array('numero' => '0805.50.00','producto' => 'Limón fresco'),
            array('numero' => '0806.20.00','producto' => 'Pasas de Uva'),
            array('numero' => '0810.40.00','producto' => 'Arándanos'),
            array('numero' => '0903.00.10','producto' => 'Yerba Mate fraccionada'),
            array('numero' => '0903.00.90','producto' => 'Yerba Mate fraccionada'),
            array('numero' => '1509.10.00','producto' => 'Aceite Oliva Virgen Extra'),
            array('numero' => '1512.19.11','producto' => 'Aceite refinado de Girasol'),
            array('numero' => '1512.19.19','producto' => 'Aceite refinado de Girasol'),
            array('numero' => '2002.10.00','producto' => 'Pulpa de tomates'),
            array('numero' => '2007.99.10','producto' => 'Mermelada de frutas'),
            array('numero' => '2008.19.00','producto' => 'Pistacho tostado y salado con cascara'),
            array('numero' => '2009.19.00','producto' => 'Pistacho pelado y tostado'),
            array('numero' => '2501.00.11','producto' => 'Sal Fina, entrefina y gruesa'),
            array('numero' => '2501.00.19','producto' => 'Sal Fina, entrefina y gruesa'),
            array('numero' => '2501.00.20','producto' => 'Sal Fina, entrefina y gruesa'),
            array('numero' => '2501.00.90','producto' => 'Sal Fina, entrefina y gruesa')
          );


        DB::table('parancelarias')->insert($parancelarias);
    }
}
