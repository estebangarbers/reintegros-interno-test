<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/login', function() {
    return view('login');
});

Route::post('/checkUser', 'SolicitudController@checkUser')->name('solicitud.checkuser');
Route::middleware(['CheckCuit'])->group(
	function ()
	{
		Route::any('/', 'SolicitudController@inicio')->name('solicitud.inicio');
		Route::post('solicitud/view', 'SolicitudController@view')->name('solicitud.view');
		Route::post('solicitud/certificado', 'SolicitudController@certificado')->name('solicitud.certificado');
		Route::resource('solicitud', 'SolicitudController');

		Route::get('/empresas/nueva', 'EmpresaController@nueva')->name('empresas.nueva');
		Route::any('/empresas', 'EmpresaController@index')->name('empresas');
		Route::get('empresas/{id}', 'EmpresaController@detalles')->name('empresas.detalles');
		Route::get('empresas/{id}/producto/{producto_id}', 'EmpresaController@productoDetalle')->name('empresas.producto.detalles');
		Route::resource('empresa', 'EmpresaController');
		Route::get('empresas/{id}/editar', 'EmpresaController@edit')->name('empresas.edit');
		Route::post('empresas/update', 'EmpresaController@update')->name('empresa.update');
		
		Route::get('empresas/{id}/establecimientos', 'EstablecimientoController@index')->name('empresas.establecimientos');
		Route::get('empresas/{id}/solicitudes', 'EmpresaController@solicitudes')->name('empresas.solicitudes');
		Route::get('empresas/{id}/establecimientos/nuevo', 'EstablecimientoController@create')->name('empresas.establecimientos.nuevo');
		Route::post('empresas/establecimientos/store', 'EstablecimientoController@store')->name('empresa.establecimiento.store');
		Route::get('empresas/{id}/establecimientos/{id_est}/editar', 'EstablecimientoController@edit')->name('empresas.establecimientos.edit');
		Route::post('empresas/establecimientos/update', 'EstablecimientoController@update')->name('empresa.establecimiento.update');
		Route::get('empresas/establecimientos/{id}/delete', 'EstablecimientoController@destroy')->name('empresa.establecimiento.destroy');

		Route::get('empresas/{id}/productos', 'ProductoController@index')->name('empresas.productos');
		Route::get('empresas/{id}/productos/nuevo', 'ProductoController@create')->name('empresas.productos.nuevo');
		Route::post('empresas/productos/store', 'ProductoController@store')->name('empresa.producto.store');
		Route::get('empresas/{id}/productos/{id_est}/editar', 'ProductoController@edit')->name('empresas.productos.edit');
		Route::post('empresas/productos/update', 'ProductoController@update')->name('empresa.producto.update');
		Route::get('empresas/productos/{id}/delete', 'ProductoController@destroy')->name('empresa.producto.destroy');

		Route::any('/parancelarias', 'ParancelariaController@index')->name('parancelarias');
		Route::get('/parancelarias/nueva', 'ParancelariaController@nueva')->name('parancelarias.nueva');
		Route::post('parancelarias/store', 'ParancelariaController@store')->name('parancelarias.store');
		Route::get('parancelarias/{id}/editar', 'ParancelariaController@edit')->name('parancelarias.edit');
		Route::post('parancelarias/update', 'ParancelariaController@update')->name('parancelarias.update');
		Route::get('parancelarias/{id}/delete', 'ParancelariaController@destroy')->name('parancelarias.destroy');

	}
);
