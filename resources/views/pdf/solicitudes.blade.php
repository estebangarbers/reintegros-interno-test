@extends('layouts.pdf')

@section('content')
    <div class="cuerpo">
        <p>DIRECCIÓN NACIONAL DE ALIMENTOS Y BEBIDAS</p>
        <p>Tengo el agrado de dirigirme a usted, a los fines de solicitar la emisión del certificado a que se refiere la Resolución MINAGRO N° 90-E/2017, para ser presentado ante la DIRECCIÓN GENERAL DE ADUANAS (DGA) conforme a lo establecido en el Decreto N° 1341 de fecha 30 de diciembre de 2016.</p>
        <p>Quien suscribe, {{mb_strtoupper($solicitud->empresa->razon_social, 'UTF-8')}}, CUIT N° {{$solicitud->empresa->cuit}}, con domicilio en {{$solicitud->domicilio}}{{($solicitud->localidad ? ', '.$solicitud->localidad->nombre : '')}}, {{$solicitud->provincia->nombre}}, declara que resulta beneficiario del derecho de uso del Sello "ALIMENTOS ARGENTINOS UNA ELECCIÓN NATURAL", y su versión en idioma inglés, "ARGENTINE FOOD A NATURAL CHOICE". Para el Producto {{mb_strtoupper($solicitud->producto->nombre, 'UTF-8')}}, Registro de Producto {{$solicitud->producto->tipo_registro}}{{$solicitud->producto->nrproducto}}, Registro de Establecimiento N°{{$solicitud->nrestablecimiento}} comercializado bajo la/s marca/s {{mb_strtoupper($solicitud->producto->marca, 'UTF-8')}}. Reconocido mediante Resolución {{$solicitud->producto->nresolucion}} y se encuentra vigente.</p>
        <p><b>Producto, características y marcas de la producción</b></p>
        Destinación de exportación por la que solicita el beneficio: {{$solicitud->destinacion}}<br/>
        Peso neto en toneladas: {{str_replace('.', ',', $solicitud->cdeclarada)}}<br/>
        Valor FOB expresado en Dólares Estadounidenses: U$S {{str_replace('.', ',', $solicitud->vfob)}}<br/>
        País de destino: {{$solicitud->pais_destino->nombre}}<br/>
        Posición arancelaria a 8 dígitos con las siglas NCM: {{$solicitud->parancelaria}}<br/>
        Descripción de la mercadería: {{$solicitud->descripcion}}<br/><br/>
        Correo electrónico: {{$solicitud->email}}<br/>
        N° de teléfono: {{$solicitud->telefono}}<br/>
        Fecha: {{$solicitud->created_at}}<br/>
        N° solicitud: {{str_pad($solicitud->id, 8, "0", STR_PAD_LEFT)}}<br/>
        </div>
    </div>
@endsection