<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Reintegros') }}</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/spinner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/selectize.default.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>  

    <link rel="stylesheet" type="text/css" href="{{ asset('css/multi-select.css') }}">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jqueryui.js') }}"></script>
    <script src="{{ asset('dist/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('js/index.js') }}"></script>
    <style type="text/css" media="screen">
        .pagination {
  display: -ms-flexbox;
  display: flex;
  padding-left: 0;
  list-style: none;
  border-radius: 0.25rem;
}
.pagination>li span {
    position: relative;
    float: left;
    padding: 0.5rem 0.75rem;
    margin-left: -1px;
    color: #fff;
    text-decoration: none;
    background-color: #999;
    border: 1px solid #ddd;
}
.page-item:first-child .page-link {
  margin-left: 0;
  border-top-left-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}

.page-item:last-child .page-link {
  border-top-right-radius: 0.25rem;
  border-bottom-right-radius: 0.25rem;
}

.page-item.active .page-link {
  z-index: 2;
  color: #fff;
  background-color: #007bff;
  border-color: #007bff;
}

.page-item.disabled .page-link {
  color: #868e96;
  pointer-events: none;
  background-color: #fff;
  border-color: #ddd;
}

.page-link {
  position: relative;
  display: block;
  padding: 0.5rem 0.75rem;
  margin-left: -1px;
  line-height: 1.25;
  color: #007bff;
  background-color: #fff;
  border: 1px solid #ddd;
}

.page-link:focus, .page-link:hover {
  color: #0056b3;
  text-decoration: none;
  background-color: #e9ecef;
  border-color: #ddd;
}

.pagination-lg .page-link {
  padding: 0.75rem 1.5rem;
  font-size: 1.25rem;
  line-height: 1.5;
}

.pagination-lg .page-item:first-child .page-link {
  border-top-left-radius: 0.3rem;
  border-bottom-left-radius: 0.3rem;
}

.pagination-lg .page-item:last-child .page-link {
  border-top-right-radius: 0.3rem;
  border-bottom-right-radius: 0.3rem;
}

.pagination-sm .page-link {
  padding: 0.25rem 0.5rem;
  font-size: 0.875rem;
  line-height: 1.5;
}

.pagination-sm .page-item:first-child .page-link {
  border-top-left-radius: 0.2rem;
  border-bottom-left-radius: 0.2rem;
}

.pagination-sm .page-item:last-child .page-link {
  border-top-right-radius: 0.2rem;
  border-bottom-right-radius: 0.2rem;
}
    </style>
</head>
<body>
<div class="main-wrapper">
    <header class="header">
    <div class="header-block header-block-collapse hidden-lg-up">
        <button class="collapse-btn" id="sidebar-collapse-btn">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <div class="header-block-search hidden-sm-down">
        <h3 style="margin:0;">Sistema interno Reintegros</h3>
    </div>
    <div class="header-block header-block-nav">
        <ul class="nav-profile">
            <li class="profile dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <!--<div class="img" style="background-image: url('assets/genericuser.png')"></div>-->
                    <span class="name"> {{ Session::get('sesion_email')}} </span>
                </a>
                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item" href="/HomeAlimentos/Publicaciones/intranet/">
                    <i class="fa fa-arrow-left icon"></i> Volver a intranet </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{url('/?logout=true')}}">
                    <i class="fa fa-power-off icon"></i> Cerrar sesión </a>
                </div>
            </li>
        </ul>
    </div>
</header>
    <div class="app sidebar-fixed" id="app">

    <aside class="sidebar">
        <div class="sidebar-container">
            <div class="sidebar-header">
                <div class="brand">
                    <img src="http://www.alimentosargentinos.gob.ar/HomeAlimentos/Capacitacion/cursos-virtuales/curso-igdo/dist/img/logo_ministerio.png" style="max-width:87%;">
                </div>
            </div>
            <nav class="menu">
                <ul class="nav metismenu" id="sidebar-menu">
                    <li {!! (Request::segment(1) == '' ? "class='active'" : '') !!}>
                        <a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio </a>
                    </li>
                    <li {!! (Request::segment(1) == 'empresas' ? "class='active open'" : '') !!}>
                    <a style="cursor:pointer;"><i class="fa fa-industry" aria-hidden="true"></i> Empresas <i class="fa arrow"></i></a>
                        <ul>
                            <li>
                                <a href="{{route('empresas')}}"><i class="fa fa-list"></i> Listado </a>
                            </li>
                            <li>
                                <a href="{{route('empresas.nueva')}}"><i class="fa fa-plus"></i> Nueva empresa </a>
                            </li>
                        </ul>
                    </li>

                    <!--<li {!! (Request::segment(1) == 'busqueda' ? "class='active'" : '') !!}>
                        <a href="{{ url('/') }}"><i class="fa fa-search"></i> Búsqueda avanzada </a>
                    </li>-->

                    <li {!! (Request::segment(1) == 'parancelarias' ? "class='active open'" : '') !!}>
                    <a style="cursor:pointer;"><i class="fa fa-ship" aria-hidden="true"></i> Pos. arancelarias <i class="fa arrow"></i></a>
                        <ul>
                            <li>
                                <a href="{{route('parancelarias')}}"><i class="fa fa-list"></i> Listado </a>
                            </li>
                            <li>
                                <a href="{{route('parancelarias.nueva')}}"><i class="fa fa-plus"></i> Nueva posición </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
        <article class="content items-list-page">
            <div class="row">
                @include('layouts.breadcrumb')   

                @yield('content')           
            </div>
        </article>

    </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('js/locales/bootstrap-datetimepicker.es.js') }}"></script>
    <script type="text/javascript">
        $('.form_date').datetimepicker({
            language:  'es',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            $('#cuit').mask("00-00000000-0", {selectOnFocus: true});
            $('#numero').mask("0000.00.00", {selectOnFocus: true});

            if ($('#tipo_registro').val() != ''){
                $('#nrproducto').attr('disabled',false);
            }else{
                $('#nrproducto').attr('disabled',true);
            }

            $('#tipo_registro').on('change', function() {
                if ($('#tipo_registro').val() != ''){
                    $('#nrproducto').attr('disabled',false);
                }else{
                    $('#nrproducto').attr('disabled',true);
                }
            });
        });
    </script>

</body>
</html>