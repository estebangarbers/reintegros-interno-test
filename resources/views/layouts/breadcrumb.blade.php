<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="">Inicio</a>
    </li>
    @if(Request::segment(1) == 'empresas')
    <li class="breadcrumb-item"><a href="{{url('/empresas')}}">Empresas</a></li>
    @endif
    @if(Request::segment(1) == 'parancelarias')
    <li class="breadcrumb-item"><a href="{{url('/parancelarias')}}">Posiciones arancelarias</a></li>
    @endif
    @if(Request::segment(2) == 'nueva')
    <li class="breadcrumb-item active">Nueva</li>
    @endif
    @if(is_numeric(Request::segment(2)))
    <li class="breadcrumb-item active">{{Request::segment(2)}}</li>
        @if(Request::segment(3))
            @if(Request::segment(3) == 'edit')
                <li class="breadcrumb-item active">Editar</li>
            @elseif(Request::segment(3) == 'delete')
                <li class="breadcrumb-item active">Eliminar</li>
            @endif
        @else
            <li class="breadcrumb-item active">Detalles</li>
        @endif
    @endif
</ol>