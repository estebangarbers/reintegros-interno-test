@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> Posiciones arancelarias</h3>
                            <p class="title-description"> Posiciones arancelarias registradas en el sistema </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            @if($parancelarias)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Número</th>
                            <th>Producto</th>
                            <th width="1%"></th>
                            <th width="1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($parancelarias as $parancelaria)
                        <tr>
                            <td>{{$parancelaria->numero }}</td>
                            <td>{{$parancelaria->producto }}</td>
                            <td><a href="{{route('parancelarias.edit',['id' => $parancelaria->id])}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Editar</a></td>
                            <td><a href="{{route('parancelarias.destroy',['id' => $parancelaria->id])}}" class="btn btn-danger btn-sm" onclick='return confirm("¿Está seguro que desea eliminar?");'><i class="fa fa-trash"></i> Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$parancelarias->render()}}
            @else
                <div class="alert alert-warning">
                    No hay posiciones arancelarias registradas.
                </div>
            @endif
            </div>
        </div>
    </div>
@endsection
