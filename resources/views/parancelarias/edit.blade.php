@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> {{$parancelaria->numero}} - {{$parancelaria->producto}}
                            <a href="{{route('parancelarias')}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Volver</a></h3>
                            <p class="title-description"> Editar posición arancelaria en el sistema </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            {!! Form::open(['route' => 'parancelarias.update', 'method' => 'POST', 'id' => 'form_parancelaria']) !!}
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('numero', 'Número') }}
                        {{ Form::text('numero', $parancelaria->numero, ['class' => 'form-control', 'id' => 'numero', 'required']) }}
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        {{ Form::label('producto', 'Producto') }}
                        {{ Form::text('producto', $parancelaria->producto, ['class' => 'form-control','id' => 'producto', 'required']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    {{Form::hidden('parancelaria_id', $parancelaria->id)}}
                    {{ Form::submit('Editar posición arancelaria', ['class' => 'btn btn-warning m-l-1 pull-right']) }}
                </div>
            {{Form::close()}}
            </div>            
        </div>
    </div>
@endsection
