@extends('layouts.app')

@section('content')

    
    <div class="row">
        
        @if (session('status'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        @endif

        <div class="col-md-4">
            <div class="alert alert-primary">
                <h4>Empresas:</h4>
                <p><i class="fa fa-industry"></i> {{$cempresas}}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="alert" style="margin:0;background:#757D75;color:#FFFFFF;">
                <h4>Solicitudes:</h4>
                <p><i class="fa fa-file-alt"></i> {{$csolicitudes}}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="alert alert-gray">
                <h4>Expedientes:</h4>
                <p><i class="fa fa-book"></i> {{$cexpedientes}}</p>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-block">
            <h4>Últimas solicitudes registradas:</h4>
            <br>
            @if($solicitudes)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>Fecha</th>
                            <th>Razón social</th>
                            <th>N° GEDO</th>
                            <th width="1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($solicitudes as $solicitud)
                        <tr>
                            <td>{{$solicitud->id }}</td>
                            <td>{{$solicitud->created_at->format('d/m/Y H:i') }}hs</td>
                            <td>{{$solicitud->empresa->razon_social }}</td>
                            <td>{{(!empty($solicitud->gedo) ? $solicitud->gedo->numero : '') }}</td>
                            <td><a href="{{route('empresas.detalles',['id' => $solicitud->empresa->id])}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning">
                    No hay solicitudes registradas.
                </div>
            @endif
            </div>
        </div>
    </div>
@endsection
