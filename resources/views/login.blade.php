<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Reintegros') }}</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/spinner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/selectize.default.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>  

    <link rel="stylesheet" type="text/css" href="{{ asset('css/multi-select.css') }}">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jqueryui.js') }}"></script>
    <script src="{{ asset('dist/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('js/index.js') }}"></script>
</head>
<body>
<div class="container">
  <div class="col-md-4 clearfix" style="margin:30px auto;border:1px solid #ccc;padding:30px 0;float:none;">
    <div class="col-md-12">
      <h4>Reintegros</h4>
      <hr>
    </div>
    {!! Form::open(['route' => 'solicitud.checkuser', 'method' => 'POST', 'id' => 'form_empresa']) !!}
    <div class="col-md-12">
      <div class="form-group">
        <label>Correo electrónico:</label>
        <input type="email" name="email" class="form-control" placeholder="Escriba aquí su email" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>Contraseña:</label>
        <input type="password" name="password" class="form-control" autocomplete="off" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Iniciar sesión</button>
      </div>
    </div>
    {!! Form::close() !!}
    @if (session()->has('message'))
      <div class="col-md-12">
          <div class="alert alert-{{ session('flash.class') }}">
              {{ session('message') }}
          </div>
      </div>
      @endif
  </div>
</div>
</body>
</html>