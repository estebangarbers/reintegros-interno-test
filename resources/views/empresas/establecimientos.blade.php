@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> {{$empresa->razon_social}}
                            <a href="{{route('empresas.detalles',['id' => $empresa->id])}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Volver</a>
                            </h3>
                            <p class="title-description"> Establecimientos de la empresa </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            @if(!$establecimientos->isEmpty())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>N° Establecimiento</th>
                            <th width="1%"></th>
                            <th width="1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($establecimientos as $establecimiento)
                        <tr>
                            <td>{{$establecimiento->id }}</td>
                            <td>{{$establecimiento->registro }}</td>
                            <td><a href="{{route('empresas.establecimientos.edit',['id' => $empresa->id, 'id_est' => $establecimiento->id])}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a></td>
                            <td><a href="{{route('empresa.establecimiento.destroy',['id' => $establecimiento->id])}}" class="btn btn-danger btn-sm" onclick='return confirm("¿Está seguro que desea eliminar?");'><i class="fa fa-trash"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$establecimientos->render()}}
            @else
                <div class="alert alert-warning">
                    La empresa no tiene establecimientos registrados.
                </div>
            @endif
            </div>
        </div>
    </div>
@endsection
