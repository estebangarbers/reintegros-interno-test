@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> Nueva empresa</h3>
                            <p class="title-description"> Registrar nueva empresa en el sistema </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            {!! Form::open(['route' => 'empresa.store', 'method' => 'POST', 'id' => 'form_empresa']) !!}
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('razon_social', 'Razón social') }}
                        {{ Form::text('razon_social', null, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('cuit', 'CUIT') }}
                        {{ Form::text('cuit', null, ['class' => 'form-control','id' => 'cuit', 'required']) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('activo', 'Estado') }}
                        {{ Form::select('activo', [1 => 'Activa', 0 => 'Inactiva'], 1, ['class' => 'form-control', 'id' => 'activo']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    {{ Form::submit('Registrar empresa', ['class' => 'btn btn-success m-l-1 pull-right']) }}
                </div>
            {{Form::close()}}
            </div>            
        </div>
    </div>
@endsection
