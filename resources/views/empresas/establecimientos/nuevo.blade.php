@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> Nuevo establecimiento
                            <a href="{{route('empresas.detalles',['empresa_id' => $empresa->id])}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Volver</a>
                            </h3>
                            <p class="title-description"> Registrar nueva establecimiento en el sistema </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            {!! Form::open(['route' => 'empresa.establecimiento.store', 'method' => 'POST', 'id' => 'form_establecimiento']) !!}
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('nrestablecimiento', 'N° Registro del Establecimiento') }}
                        {{ Form::text('nrestablecimiento', null, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    {{ Form::hidden('empresa_id', $empresa->id) }}
                    {{ Form::submit('Registrar establecimiento', ['class' => 'btn btn-success m-l-1 pull-right']) }}
                </div>
            {{Form::close()}}
            </div>            
        </div>
    </div>
@endsection
