@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> Empresas</h3>
                            <p class="title-description"> Empresas registradas en el sistema </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            @if($empresas)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>Razón social</th>
                            <th>CUIT</th>
                            <th width="1%">Solicitudes</th>
                            <th width="1%">Estado</th>
                            <th width="1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($empresas as $empresa)
                        <tr>
                            <td>{{$empresa->id }}</td>
                            <td>{{$empresa->razon_social }}</td>
                            <td>{{$empresa->cuit }}</td>
                            <td>{{count($empresa->solicitudes)}}</td>
                            <td>{!! ($empresa->activo == 1 ? '<span class="label label-success" style="font-size:0.6em;padding:5px 7px;">Activa</span>' : '<span class="label label-warning" style="font-size:0.6em;padding:5px 7px;">Inactiva</span>') !!}</td>
                            <td><a href="{{route('empresas.detalles',['id' => $empresa->id])}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i></button></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$empresas->render()}}
            @else
                <div class="alert alert-warning">
                    No hay empresas registradas.
                </div>
            @endif
            </div>
        </div>
    </div>
@endsection
