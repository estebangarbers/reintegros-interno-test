@extends('layouts.app')

@section('content')

    <style type="text/css" media="screen">
        .table{
            font-size:0.8em;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> {{$empresa->razon_social}}
                            <a href="{{route('empresas.detalles',['id' => $empresa->id])}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Volver</a>
                            </h3>
                            <p class="title-description"> Solicitudes de la empresa </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            @if(!$solicitudes->isEmpty())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th>N° GEDO</th>
                            <th>N° Expediente</th>
                            <th>Fecha registro</th>
                            <th width="1%"></th>
                            <th width="1%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($solicitudes as $solicitud)
                        <tr>
                            <td>{{$solicitud->id }}</td>
                            <td>{{(!empty($solicitud->gedo) ? $solicitud->gedo->numero : '') }}</td>
                            <td>{{(!empty($solicitud->sesion->expediente) ? $solicitud->sesion->expediente->nroCompleto : '') }}</td>
                            <td>{{$solicitud->created_at->format('d/m/Y H:i') }}hs</td>
                            <td>
                                {!! Form::open(['method' => 'POST', 'route' => 'solicitud.view', 'style' => 'margin:0']) !!}
                                <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
                                <button type="submit" class="btn btn-warning btn-sm" style="margin:0;"><i class="fa fa-download"></i> Solicitud</button>
                                {!! Form::close() !!}
                            </td>
                            <td>
                                {!! Form::open(['method' => 'POST', 'route' => 'solicitud.certificado', 'style' => 'margin:0']) !!}
                                <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
                                <button type="submit" class="btn btn-primary btn-sm" style="margin:0;"><i class="fa fa-download"></i> Certificado</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$solicitudes->render()}}
            @else
                <div class="alert alert-warning">
                    La empresa no tiene establecimientos registrados.
                </div>
            @endif
            </div>
        </div>
    </div>
@endsection
