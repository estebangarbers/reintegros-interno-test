@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> Nuevo producto
                            <a href="{{route('empresas.detalles',['empresa_id' => $empresa->id])}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Volver</a>
                            </h3>
                            <p class="title-description"> Registrar nuevo producto en el sistema </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            {!! Form::open(['route' => 'empresa.producto.store', 'method' => 'POST', 'id' => 'form_producto']) !!}
                <div class="col-md-5">
                    <div class="form-group">
                        {{ Form::label('nombre', 'Nombre') }}
                        {{ Form::text('nombre', null, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        {{ Form::label('marca', 'Marca') }}
                        {{ Form::text('marca', null, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Fecha de emisión:</label>
                        <input type="text" name="fecha" id="fecha" class="form-control date form_date" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy" required="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('tipo_registro', 'Tipo de registro') }}
                        {{ Form::select('tipo_registro', ['RENSPA N°' => 'RENSPA N°','RENPA N°' => 'RENPA N°'], '', ['class' => 'form-control','placeholder' => 'Sin número de registro', 'id' => 'tipo_registro']) }}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('certificacion', 'Certificación') }}
                        {{ Form::select('certificacion', [1 => 'Sello Alimentos Argentinos', 2 => 'IG-DO'], '', ['class' => 'form-control','placeholder' => 'Seleccione una certificación', 'required']) }}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('nrproducto', 'N° registro') }}
                        {{ Form::text('nrproducto', null, ['class' => 'form-control', 'id' => 'nrproducto']) }}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('nresolucion', 'N° de resolución') }}
                        {{ Form::text('nresolucion', null, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                
                <div class="col-md-12">
                    {{ Form::hidden('empresa_id', $empresa->id) }}
                    {{ Form::submit('Registrar producto', ['class' => 'btn btn-success m-l-1 pull-right']) }}
                </div>


            {{Form::close()}}
            </div>            
        </div>
    </div>
@endsection
