@extends('layouts.app')

@section('content')
    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;border:none;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title">{{$empresa->id}} - {!! ($empresa->activo == 1 ? '<span class="label label-success" style="font-size:0.6em;padding:5px 7px;">Activa</span>' : '<span class="label label-warning" style="font-size:0.6em;padding:5px 7px;">Inactiva</span>') !!} {{$empresa->razon_social}}
                            <a href="{{route('empresas.edit',['id' => $empresa->id])}}" class="btn btn-warning pull-right"><i class="fa fa-edit"></i> Editar</a></h3>
                            <p class="title-description"> CUIT: {{$empresa->cuit}} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
                <h5>Establecimientos de la empresa 
                    <a href="{{route('empresas.establecimientos.nuevo',['empresa_id' => $empresa->id])}}" class="btn btn-sm btn-success pull-right">Nuevo establecimiento</a>
                    <br>
                    <small>Total: {{ count($establecimientos) }} establecimientos</small>
                </h5>
                @if(!$establecimientos->isEmpty())
                    <table class="table">
                        <thead>
                            <tr>
                                <th>N° de registro</th>
                                <th width="1%"></th>
                                <th width="1%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($establecimientos as $establecimiento)
                            <tr>
                                <td>{{$establecimiento->registro}}</td>
                            </tr>                            
                            @endforeach
                        </tbody>                        
                    </table>
                    <a href="{{route('empresas.establecimientos',['empresa_id' => $empresa->id])}}" class="btn btn-block btn-secondary">Ver todos los establecimientos</a>
                @else
                <div class="alert alert-warning">La empresa no posee establecimientos registrados.</div>
                @endif
            </div>

            <div class="card card-block">
                <h5>Productos de la empresa
                    <a href="{{route('empresas.productos.nuevo',['empresa_id' => $empresa->id])}}" class="btn btn-sm btn-success pull-right">Nuevo producto</a>
                    <br>
                    <small>Total: {{ count($empresa->productos) }} productos</small>
                </h5>
                @if(!$productos->isEmpty())
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="1%">#</th>
                                <th>Producto</th>
                                <th>Marca</th>
                                <th>N° Resolución</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($productos as $producto)
                            <tr>
                                <td>{{$producto->id}}</td>
                                <td>{{$producto->nombre}}</td>
                                <td>{{$producto->marca}}</td>
                                <td>{{$producto->nresolucion}}</td>
                            </tr>                            
                            @endforeach
                        </tbody>                        
                    </table>
                    <a href="{{route('empresas.productos',['empresa_id' => $empresa->id])}}" class="btn btn-block btn-secondary">Ver todos los productos</a>
                @else
                <div class="alert alert-warning">La empresa no posee productos registrados.</div>
                @endif
            </div>
            <div class="card card-block">
                <h5>Solicitudes de la empresa <br><small>Total: {{ count($empresa->solicitudes) }} solicitudes</small></h5>
                @if(!$solicitudes->isEmpty())
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="1%">ID</th>
                                <th>Fecha</th>
                                <th>N° GEDO</th>
                                <th width="1%"></th>
                                <th width="1%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudes as $solicitud)
                            <tr>
                                <td>{{$solicitud->id}}</td>
                                <td>{{$solicitud->created_at->format('d/m/Y H:i')}}hs</td>
                                <td>{{(!empty($solicitud->gedo) ? $solicitud->gedo->numero : '') }}</td>
                                <td>
                                    {!! Form::open(['method' => 'POST', 'route' => 'solicitud.view', 'style' => 'margin:0']) !!}
                                    <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
                                    <button type="submit" class="btn btn-warning btn-sm" style="margin:0;"><i class="fa fa-download"></i> Solicitud</button>
                                    {!! Form::close() !!}
                                </td>
                                <td>
                                    {!! Form::open(['method' => 'POST', 'route' => 'solicitud.certificado', 'style' => 'margin:0']) !!}
                                    <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
                                    <button type="submit" class="btn btn-primary btn-sm" style="margin:0;"><i class="fa fa-download"></i> Certificado</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>                            
                            @endforeach
                        </tbody>                        
                    </table>
                    <a href="{{route('empresas.solicitudes',['empresa_id' => $empresa->id])}}" class="btn btn-block btn-secondary">Ver todas las solicitudes</a>
                @else
                <div class="alert alert-warning">La empresa no posee solicitudes registradas.</div>
                @endif
                <hr>
            </div>
        </div>
    </div>
@endsection
