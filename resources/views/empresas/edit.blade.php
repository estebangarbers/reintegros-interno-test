@extends('layouts.app')

@section('content')

    
    <div class="row">
        <div class="col-md-12">
            <div class="title-search-block">
                <div class="title-block" style="margin-bottom:0;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title"> {{ $empresa->razon_social }}
                            <a href="{{route('empresas.detalles',['empresa_id' => $empresa->id])}}" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Volver</a></h3>
                            <p class="title-description"> Editar empresa </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('message'))
        <div class="col-md-12">
            <div class="alert alert-{{ session('flash.class') }}">
                {{ session('message') }}
            </div>
        </div>
        @endif
        
        <div class="col-md-12">
            <div class="card card-block">
            {!! Form::open(['route' => 'empresa.update', 'method' => 'POST', 'id' => 'form_empresa']) !!}
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('razon_social', 'Razón social') }}
                        {{ Form::text('razon_social', $empresa->razon_social, ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('cuit', 'CUIT') }}
                        {{ Form::text('cuit', $empresa->cuit, ['class' => 'form-control','id' => 'cuit', 'required']) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('activo', 'Estado') }}
                        {{ Form::select('activo', [1 => 'Activa', 0 => 'Inactiva'], $empresa->activo, ['class' => 'form-control', 'id' => 'activo']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    {{ Form::hidden('empresa_id', $empresa->id) }}
                    {{ Form::submit('Editar empresa', ['class' => 'btn btn-warning m-l-1 pull-right']) }}
                </div>
            {{Form::close()}}
            </div>            
        </div>
    </div>
@endsection
